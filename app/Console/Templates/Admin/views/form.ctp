<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><?php echo "$title"; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<?php echo "<?php echo \$this->Form->create('{$modelClass}', array('role' => 'form')); ?>\n"; ?>
  <div class="box-body">
		<?php
			foreach ($fields as $field) {
				if (strpos($action, 'add') !== false && $field == $primaryKey) {
					continue;
				} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
					echo "\t\t\t\t\t<div class=\"form-group\">\n";
					echo "\t\t\t\t\t\t<?php echo \$this->Form->input('{$field}', array('label'=>'" . ucfirst($field) .":', 'class'=>'form-control', 'placeholder'=>'{$field}'));?>\n";
					echo "\t\t\t\t\t</div>\n";
				}
			}
			if (!empty($associations['hasAndBelongsToMany'])) {
				foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
					echo "\t\t\t\t\t<div class=\"form-group\">\n";
					echo "\t\t\t\t\t\t\t<?php echo \$this->Form->input('{$assocName}');?>\n";
					echo "\t\t\t\t\t</div>\n";
				}
			}
		?>
  </div>
  <?php
    echo "\n";
    echo "\t\t\t\t\t<div class=\"box-footer\">\n";
    echo "\t\t\t\t\t\t<?php echo \$this->Form->button('<i class=\"glyphicon glyphicon-saved\"></i> Save data', array('type' => 'submit', 'class' => 'btn btn-large btn-primary', 'escape' => false)); ?>\n";
    echo "\t\t\t\t\t</div>\n";
   ?>
  <?php echo "<?php echo \$this->Form->end(); ?>\n";?>
</div>
