<?php
App::uses('AppModel', 'Model');
/**
 * Segment Model
 *
 * @property Company $Company
 */
class Segment extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'unique' => array(
        'rule' => 'isUnique',
        'message' => 'Já existe um segmento cadastrado com esta nome. Por favor, insira outro.'
       )
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Company' => array(
			'className' => 'Company',
			'foreignKey' => 'segment_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => 'Company.title ASC',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
