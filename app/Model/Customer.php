<?php
App::uses('AppModel', 'Model');
/**
 * Customer Model
 *
 * @property Company $Company
 */
class Customer extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'titular';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'titular' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'unique' => array(
        'rule' => 'isUnique',
        'message' => 'Já existe um cliente cadastrado com esta titularidade. Por favor, insira outro.'
       )
		)
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Company' => array(
			'className' => 'Company',
			'foreignKey' => 'customer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
