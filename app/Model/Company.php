<?php
App::uses('AppModel', 'Model');
/**
 * Company Model
 *
 * @property Customer $Customer
 * @property Segment $Segment
 * @property Plan $Plan
 * @property District $District
 */
class Company extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
		 	'notBlank' => array(
				 'rule' => array('notBlank'),
				 //'message' => 'Your custom message here',
				 //'allowEmpty' => false,
				 //'required' => false,
				 //'last' => false, // Stop validation after this rule
				 //'on' => 'create', // Limit validation to 'create' or 'update' operations
		 	),
			'unique' => array(
				 'rule' => 'isUnique',
				 'message' => 'Já existe uma Companhia/Comércio com este nome.'
			)
	 	),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Segment' => array(
			'className' => 'Segment',
			'foreignKey' => 'segment_id',
			'conditions' => ['Segment.active'=> 0 ],
			'fields' => '',
			'order' => ''
		),
		'Plan' => array(
			'className' => 'Plan',
			'foreignKey' => 'plan_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'District' => array(
			'className' => 'District',
			'foreignKey' => 'district_id',
			'conditions' => ['District.active'=> 0 ],
			'fields' => '',
			'order' => ''
		)
	);


	public function beforeDelete ($options = array()) {
		$registro = $this->findById( $this->id );
		if(isset($registro[$this->name]['ext'])){
			$modulo = $this->name;
			$file = new File ( APP.WEBROOT_DIR.DS."img" . "/{$this->name}/{$registro[$this->name]['id']}.{$registro[$this->name]['ext']}" );
			if( $file->exists() ){
				$file->delete();
			}
		}
	}

}
