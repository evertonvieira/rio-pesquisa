<script type="text/javascript">
//<![CDATA[
	var BASE_URL =  "<?php echo Router::url('/', true); ?>";
//]]>
</script>
<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">
			<?php echo __d('O', 'Imagens da galeria'); ?> <?=$parent_model_str;?> <?=$parent_record[$parent_model_str]['id'];?>
		</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>


	<div class="box-body">
		<div class="box-tools">
			<?php
				// Executa o helper Plupload para criar o furmulário
				$options = array();
				$options['parent_model_str'] = $parent_model_str;
				$options['parent_controller_str'] = $parent_controller_str;
				$options['parent_record_id'] = $parent_record[$parent_model_str]['id'];
				echo $this->Plupload->form( $options );
			?>
		</div>

		<div id="post">
	    <?php echo $this->element("image_list"); ?>
	  </div>

	</div>
</div>
