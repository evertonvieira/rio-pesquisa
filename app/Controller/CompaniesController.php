<?php
App::uses('AppController', 'Controller');
/**
 * Companies Controller
 *
 * @property Company $Company
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class CompaniesController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->title = 'Companhias/Comércios';
    $this->description = '';
		$this->Company->recursive = 0;
		$companies = $this->Company->find('all');
		$this->set(compact('companies'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
    $this->title = 'Companhias/Comércios';
    $this->description = '';
		$this->Company->recursive = 2;
		if (!$this->Company->exists($id)) {
			throw new NotFoundException(__('Invalid company'));
		}
		$options = array('conditions' => array('Company.' . $this->Company->primaryKey => $id));
		$this->set('company', $this->Company->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */

	public function admin_add() {
		$this->title = 'Companhias/Comércios';
		if ($this->request->is('post')) {
			if (!empty($this->request->data['Company']['imagem']['name'])){

				// Valida os dados da imagem com relação ao banco de dados
				$this->Company->set($this->request->data);
				if( !$this->Company->validates() ) {
					$result = 'error';
				}

				// Valida a imagem no componente
				$valida = $this->Upload->validate(
					$this->request->data['Company']['imagem'],
					array('image/*')
				);
				if(!$valida){
					$result = 'error';
				}
				if($valida){
					$this->Company->create();
					$this->request->data['Company']['ext'] = $this->Upload->get_extension($this->request->data['Company']['imagem']);
					if (!empty($this->request->data['Company']['mobile']['name'])) {
						$this->request->data['Company']['img_mobile'] = $this->Upload->get_extension($this->request->data['Company']['mobile']);
					}
					if ($this->Company->save( $this->request->data ) ) {
						if (!empty($this->request->data['Company']['mobile']['name'])) {
							$upload = $this->Upload->image($this->request->data['Company']['mobile'], ['name'=>'mobile_'.$this->Company->id, 'dir'=>'Company/'.$this->Company->id] );
						}
						$options = array(
							'name'    => $this->Company->id,
							'dir'     => 'Company/'.$this->Company->id,
						);
						$upload = $this->Upload->image($this->request->data['Company']['imagem'], $options);
						$this->Session->setFlash(__('A Companhia/Comércios foi <strong>cadastrada</strong> com sucesso!'), 'flash/success');
						$this->redirect(array('action' => 'add'));
					} else {
						$this->Session->setFlash(__('Erro ao <strong>cadastrar</strong> a Comanhia/Comércio. Por favor, tente novamente.'), 'flash/error');
					}
				}
			}else{
				$this->Company->create();
				if ($this->Company->save($this->request->data)) {
					$this->Session->setFlash(__('A Companhia/Comércios foi <strong>cadastrada</strong> com sucesso!'), 'flash/success');
					$this->redirect(array('action' => 'add'));
				} else {
					$this->Session->setFlash(__('Erro ao <strong>cadastrar</strong> a Comanhia/Comércio. Por favor, tente novamente.'), 'flash/error');
				}
			}
		}
		$customers = $this->Company->Customer->find('list');
		$segments = $this->Company->Segment->find('list', ['conditions'=>['Segment.active'=> 0] ]);
		$plans = $this->Company->Plan->find('list');
		$districts = $this->Company->District->find('list', ['conditions'=>['District.active'=> 0] ]);
		$this->set(compact('customers', 'segments', 'plans', 'districts'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function admin_edit($id = null) {
		$this->title = 'Companhias/Comércios';
		$this->Company->id = $id;
		if (!$this->Company->exists($id)) {
			throw new NotFoundException(__('Não foi encontrado o registro para o ID especificado!'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if (!empty($this->request->data['Company']['imagem']['name'])){
				// Monta o array de dados para o banco de dados
				$this->request->data['Company']['ext'] = $this->Upload->get_extension($this->request->data['Company']['imagem']);
				// Valida os dados da imagem com relação ao banco de dados

				$this->Company->set($this->request->data);
				if( !$this->Company->validates() ) {
					$result = 'error';
				}
				// Valida a imagem no componente
				$valida = $this->Upload->validate(
					$this->request->data['Company']['imagem'],
					array('image/*')
				);
				if(!$valida){
					$result = 'error';
				}

				if($valida){
					// Monta o array de opções para o Download
					// ATENÇÃO !!! NÂO é necessário informar o diretório img
					// O diretório img já é definido em path_root
					// Caso queira que a imagem fique em outro diretório
					// informar path_root diferente: options['path_root'] = 'nove/endereco/para/imagem'
					$options = array(
						'name'    => $this->Company->id,
						'dir'     => 'Company/'.$this->Company->id,
					);
					$upload = $this->Upload->image($this->request->data['Company']['imagem'], $options);
				}
			}
			if (!empty($this->request->data['Company']['mobile']['name'])) {
				$this->request->data['Company']['img_mobile'] = $this->Upload->get_extension($this->request->data['Company']['mobile']);
				$upload = $this->Upload->image($this->request->data['Company']['mobile'], ['name'=>'mobile_'.$this->Company->id, 'dir'=>'Company/'.$this->Company->id] );
			}
			if ($this->Company->save($this->request->data)) {
				$this->Session->setFlash(__('A Companhia/Comércio foi <strong>editado</strong> com sucesso!'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Não foi possivel <strong>editar</strong> a Companhia/Comércio. Por favor, tente novamente.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Company.' . $this->Company->primaryKey => $id));
			$this->request->data = $this->Company->find('first', $options);
		}
		$customers = $this->Company->Customer->find('list');
		$segments = $this->Company->Segment->find('list', ['conditions'=>['Segment.active'=> 0] ]);
		$plans = $this->Company->Plan->find('list');
		$districts = $this->Company->District->find('list', ['conditions'=>['District.active'=> 0] ]);
		$this->set(compact('customers', 'segments', 'plans', 'districts'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Company->id = $id;
		if (!$this->Company->exists()) {
			throw new NotFoundException(__('Invalid company'));
		}
		if ($this->Company->delete()) {
			$this->Session->setFlash(__('A Companhia/Comércio foi <b>deletado</b> com sucesso!'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Erro ao <b>deletar</b> a Companhia/Comércio.'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}


/**
 * getCompanies method
 *
 * @param string $slug, $plan
 * @return void
 */
	public function getCompanies( $segmentSlug = NULL , $plan, $format = 'list', $limit = 10){
		$segment = $this->Company->Segment->findBySlug($segmentSlug);
		$this->Company->recursive = 0;
		$conditions = [
			'Company.active' => 0, 'Company.plan_id' => $plan
		];


		if ($segmentSlug != "NULL") {
			$conditions['Company.segment_id'] = $segment['Segment']['id'];
		}
		$companies = $this->Company->find($format, [
			'order' => 'RAND()',
			'limit' => $limit,
			'conditions'=> $conditions
		]);
		return $companies;
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @return void
 */
 public function view(){
	 $slug = $this->request->params['pass'][1];
	 $this->Company->recursive = 2;
	 $company = $seo = $this->Company->findBySlug($slug);

	 if ( !($company) ) {
		 throw new NotFoundException(__('Invalid company'));
	 }
	 $this->set(compact('company', 'seo'));
 }


/**
  * search method
  *
  * @throws NotFoundException
  * @return void
  */

	public function search() {
		$this->Company->recursive = 0;
		$typed_text = false;
		$filter = array( 'Company.active'=> 0 );

		$this->paginate = array(
			'order' => array('Company.title' => 'ASC'),
			'limit' => 50
		);
		if( isset($this->request->query['s']) ) {
			$this->redirect( array('action'=>'search', $this->request->query['s'], $this->request->query['bairro'] ) );
		}
		if( isset($this->request->params['pass'][0]) ){
			if( isset($this->request->params['pass'][1])){
				$filter['Company.district_id'] = $this->request->params['pass'][1];
			}
			$typed_text = $this->request->params['pass'][0];
			$keywords = explode(' ', $this->request->params['pass'][0]);
			foreach( $keywords as $keyword ){
				$keyword = trim($keyword);
				if( strlen( $keyword ) > 3 ){
					$filter['or'][] = array('Company.title LIKE' => "%{$keyword}%");
					$filter['or'][] = array('Company.description LIKE' => "%{$keyword}%");
				}
			}
		}
		$result = $this->paginate($filter);
		$companies = [];
		if ($result) {
			$companies = [ 'Gold'=>[], 'Premium'=>[], 'Bronze'=>[] ];
			foreach ($result as $key => $company) {
				switch ($company['Company']['plan_id']) {
					case 2:
						$companies['Gold'][$key] = $company;
						break;
					case 3:
						$companies['Premium'][$key] = $company;
						break;
					case 4:
						$companies['Bronze'][$key] = $company;
						break;
					default:
						break;
				}
			}
		}
		$this->set(compact('companies'));
	}


}
