<?php

App::uses('AppController', 'Controller');

class SettingsController extends AppController {
  public $uses = ['MetaTag', 'User'];

  public function admin_index() {
    $this->title = "Configurações gerais";
    $this->description = "Ajuste aqui toda as configurações necessárias para a aplicação.";
  }


  public function admin_profile(){
    $this->title = "Configurações da conta";
    $this->description = "";

    $this->User->id = $id = $this->Auth->user('id');
    if (!$this->User->exists($id)) {
      throw new NotFoundException(__('Invalid plan'));
    }
    if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Dados <b>editado</b> com sucesso!'), 'flash/success');
			} else {
				$this->Session->setFlash(__('Erro ao <b>editar</b> os dados. Por favor, verifique os erros e tente novamente.'), 'flash/error');
			}
		} else {
      $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
      $this->request->data = $this->User->find('first', $options);
		}
    $this->render('admin_index');
  }

  /**
   * admin_seo method
   * Define os metadados default para o sistema
   *
   * @throws NotFoundException
   * @return void
   */
  	public function admin_seo() {
  		$this->title = 'Configurações de SEO';
      $this->description = 'Gerencie as metatags do site.';
  		if ($this->request->is('post') || $this->request->is('put')) {
        $this->request->data['MetaTag']['model'] = 'all';
        $this->request->data['MetaTag']['foreign_key'] = 0;
  			if ($this->MetaTag->save($this->request->data)) {
  				$this->Session->setFlash(__('Dados de <strong>SEO DEFAULT</strong> foram atualizados com sucesso.'), 'flash/success');
  				$this->redirect(array('controller'=>'settings','action' => 'seo'));
  			} else {
  				$this->Session->setFlash(__('Houve um erro ao atualizar os dados de <strong>seo</strong>. Por favor, tente novamente.'), 'flash/error');
  			}
  		} else {
  			$this->request->data = $this->MetaTag->findByModel('all');
  			$this->render('admin_index');
  		}
  	}

}
