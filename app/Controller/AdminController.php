<?php

App::uses('AppController', 'Controller');

class AdminController extends AppController {

  var $uses = false;
  public function admin_index() {
    $this->title = "Dashboard";
    $this->description = "Version 1.0";
  }
  public function index() {
    $this->title = "Dashboard";
    $this->description = "Version 1.0";
		$this->admin_index;
    $this->autoRender = false;
    $this->render("admin_index");
  }

  public function beforeFilter(){
    parent::beforeFilter();
  }
}
