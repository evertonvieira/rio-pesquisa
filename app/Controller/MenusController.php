<?php
App::uses('AppController', 'Controller');
/**
 * Menus Controller
 *
 * @property Menu $Menu
 */
class MenusController extends AppController {

/**
 * load method
 *
 * @return array $menu
 */
	public function load( $id = null ) {
    $this->Menu->recursive = 2;
    if($id){
      $menu = $this->Menu->findById($id);
    }else{
      $menu = $this->Menu->find('all');
    }
    return $menu;
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->title = "Menu";
    $this->description = "Gerencie os menus do site.";
		$this->Menu->recursive = 0;
		$this->set('menus', $this->Menu->find("all"));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->title = "Menu";
		$this->description = "visualizar dados de menu.";
		if (!$this->Menu->exists($id)) {
			throw new NotFoundException(__('menu inválido'));
		}
		$options = array('conditions' => array('Menu.' . $this->Menu->primaryKey => $id));
		$this->set('menu', $this->Menu->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->title = "Menu";
		$this->description = "Gerencie os menus do site.";
		if ($this->request->is('post')) {
			$this->Menu->create();
			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('menu <b>salvo</b> com sucesso!'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('<b>Erro</b> ao salvar o menu. Pro favor, tente novamente.'), 'flash/error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->title = "Menu";
		$this->description = "Editar menu atual.";
		if (!$this->Menu->exists($id)) {
			throw new NotFoundException(__('Invalid menu'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('menu <b>editado</b> com sucesso!'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('<b>Erro</b> ao editar o menu. Pro favor, tente novamente.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Menu.' . $this->Menu->primaryKey => $id));
			$this->request->data = $this->Menu->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Menu->id = $id;
		if (!$this->Menu->exists()) {
			throw new NotFoundException(__('Invalid menu'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Menu->delete()) {
			$this->Session->setFlash(__('Menu <b>deletado</b> com sucesso!'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Erro ao <b>deletar</b>o menu.'));
		$this->redirect(array('action' => 'index'));
	}
}
