<?php
App::uses('AppController', 'Controller');
/**
 * Registrations Controller
 *
 * @property Registration $Registration
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RegistrationsController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'anuncie';
		if ($this->request->is('post')) {

			$this->Registration->create();
			$this->request->data['Registration']['ext'] = $this->Upload->get_extension($this->request->data['Registration']['imagem']);

			if ($this->Registration->save($this->request->data)) {

				$options = array(
					'name'    => $this->Registration->id,
					'dir'     => 'Registration',
				);
				$upload = $this->Upload->image($this->request->data['Registration']['imagem'], $options);

				$this->Session->setFlash(__('Suas informações foram enviadas com sucesso. O mais breve possível, entraremos en contato para passar mais detalhes.'), 'flash/success');
				$this->redirect(array('controller'=>'pages', 'action' => 'view', 'slug'=>'anuncie'));
			} else {
				$this->Session->setFlash(__('Erro ao enviar as informações. Por favor, tente novamente.'), 'flash/error');
			}
		}
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->title = '';
    $this->description = '';
		$this->Registration->recursive = 0;
		$this->set('registrations', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
    $this->title = '';
    $this->description = '';
		if (!$this->Registration->exists($id)) {
			throw new NotFoundException(__('Invalid registration'));
		}
		$options = array('conditions' => array('Registration.' . $this->Registration->primaryKey => $id));
		$this->set('registration', $this->Registration->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
    $this->title = '';
    $this->description = '';
		if ($this->request->is('post')) {
			$this->Registration->create();
			if ($this->Registration->save($this->request->data)) {
				$this->Session->setFlash(__('The registration has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The registration could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
    $this->title = '';
    $this->description = '';
		$this->Registration->id = $id;
		if (!$this->Registration->exists($id)) {
			throw new NotFoundException(__('Invalid registration'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Registration->save($this->request->data)) {
				$this->Session->setFlash(__('The registration has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The registration could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Registration.' . $this->Registration->primaryKey => $id));
			$this->request->data = $this->Registration->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Registration->id = $id;
		if (!$this->Registration->exists()) {
			throw new NotFoundException(__('Invalid registration'));
		}
		if ($this->Registration->delete()) {
			$this->Session->setFlash(__('Registration deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Registration was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
