<?php
App::uses('AppController', 'Controller');
/**
 * MenuSections Controller
 *
 * @property MenuSection $MenuSection
 */
class MenuSectionsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->title = "Menu de seção";
		$this->description = "Gerencie todas as seções de menu.";
		$this->MenuSection->recursive = 0;
		$this->set('menuSections', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->title = "Menu de seção";
		$this->description = "Ver dados do menu de seção";
		if (!$this->MenuSection->exists($id)) {
			throw new NotFoundException(__('Invalid menu section'));
		}
		$options = array('conditions' => array('MenuSection.' . $this->MenuSection->primaryKey => $id));
		$this->set('menuSection', $this->MenuSection->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->title = "Menu de seção";
		$this->description = "Gerencie todas as seções de menu.";
		if ($this->request->is('post')) {
			$this->MenuSection->create();
			if ($this->MenuSection->save($this->request->data)) {
				$this->Session->setFlash(__('O menu de seção foi <b>salvo</b> com sucesso!'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Não foi possível salvar o menu de seção. Por favor, tente novamente.'), 'flash/error');
			}
		}
		$menus = $this->MenuSection->Menu->find('list');
		$this->set(compact('menus'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->title = "Menu de seção";
		$this->description = "Editar menu de seção.";
		if (!$this->MenuSection->exists($id)) {
			throw new NotFoundException(__('Invalid menu section'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->MenuSection->save($this->request->data)) {
				$this->Session->setFlash(__('O menu foi <b>editado</b> com sucesso!'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('<b>Erro</b> ao salvar menu de seção. Por favor, tente novamente.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('MenuSection.' . $this->MenuSection->primaryKey => $id));
			$this->request->data = $this->MenuSection->find('first', $options);
		}
		$menus = $this->MenuSection->Menu->find('list');
		$this->set(compact('menus'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->MenuSection->id = $id;
		if (!$this->MenuSection->exists()) {
			throw new NotFoundException(__('Invalid menu section'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MenuSection->delete()) {
			$this->Session->setFlash(__('Menu de seção <b>deletado</b> com sucesso!'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Erro ao <b>deletar</b> o menu de seção.'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
