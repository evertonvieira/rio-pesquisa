<?php
App::uses('AppController', 'Controller');
/**
 * Outdoors Controller
 *
 * @property Outdoor $Outdoor
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class OutdoorsController extends AppController {


	/**
	 * getOutdoor method
	 *
	 * @return void
	 */

	 public function getOutdoors($slugSegment = NULL, $plan_id = 2, $position = 0 ) {

 	  $this->Outdoor->Segment->recursive = 0;
		$this->Outdoor->recursive = 0;
		$conditions = ['Outdoor.active'=> 0, 'Outdoor.position' => $position ];
 	  $segment = $this->Outdoor->Segment->findBySlug($slugSegment);
		if ($segment) {
			$conditions['Outdoor.segment_id'] = $segment['Segment']['id'];
		}
		if ($plan_id) {
 	    $conditions['Outdoor.plan_id'] = $plan_id;
 	  }
		$outdoors = $this->Outdoor->find("all", [
 	    'conditions'=> $conditions,
			'order'=> 'Outdoor.created DESC'
 	  ]);
 	  return $outdoors;
 	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->title = 'Gerencimento do Outdoor';
    $this->description = 'Crie, edite ou remova qualquer banner no site.';
		$this->Outdoor->recursive = 0;
		$this->set('outdoors', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->title = 'Gerencimento do Outdoor';
    $this->description = 'Crie, edite ou remova qualquer banner no site.';
		if (!$this->Outdoor->exists($id)) {
			throw new NotFoundException(__('Invalid outdoor'));
		}
		$options = array('conditions' => array('Outdoor.' . $this->Outdoor->primaryKey => $id));
		$this->set('outdoor', $this->Outdoor->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->title = 'Gerencimento do Outdoor';
    $this->description = 'Crie, edite ou remova qualquer banner no site.';
		if ($this->request->is('post')) {
			if (!empty($this->request->data['Outdoor']['imagem']['name'])){
				$data = [];
				$data['Outdoor']['title'] = $this->request->data['Outdoor']['title'];
				$data['Outdoor']['link'] = $this->request->data['Outdoor']['link'];
				$data['Outdoor']['ext'] = $this->Upload->get_extension($this->request->data['Outdoor']['imagem']);
				$data['Outdoor']['date'] = $this->data['Outdoor']['date'];
				$data['Outdoor']['company_id'] = $this->data['Outdoor']['company_id'];
				$data['Outdoor']['plan_id'] = $this->data['Outdoor']['plan_id'];
				$data['Outdoor']['segment_id'] = $this->data['Outdoor']['segment_id'];
				$data['Outdoor']['active'] = $this->data['Outdoor']['active'];

				// Valida os dados da imagem com relação ao banco de dados
				$this->Outdoor->set($this->request->data);
				if( !$this->Outdoor->validates() ) {
					$result = 'error';
				}

				// Valida a imagem no componente
				$valida = $this->Upload->validate(
					$this->request->data['Outdoor']['imagem'],
					array('image/*')
				);
				if(!$valida){
					$result = 'error';
				}
				if($valida){
					$this->Outdoor->create();
					if ($this->Outdoor->save( $data ) ) {
						// Monta o array de opções para o Download
						// ATENÇÃO !!! NÂO é necessário informar o diretório img
						// O diretório img já é definido em path_root
						// Caso queira que a imagem fique em outro diretório
						// informar path_root diferente: options['path_root'] = 'nove/endereco/para/imagem'
						$options = array(
							'name'    => $this->Outdoor->id,
							'dir'     => 'Outdoor',
						);
						$upload = $this->Upload->image($this->request->data['Outdoor']['imagem'], $options);
						$this->Session->setFlash(__('Outdoor <strong>cadastrado</strong> com sucesso!'), 'flash/success');
						$this->redirect(array('action' => 'index'));
					} else {
						$this->Session->setFlash(__('Erro ao <strong>cadastrar</strong> o Outdoor. Por favor, tente novamente.'), 'flash/error');
					}
				}
			}else{
				$this->Outdoor->create();
				if ($this->Outdoor->save($this->request->data)) {
					$this->Session->setFlash(__('O Outdoor foi <strong>cadastrada</strong> com sucesso!'), 'flash/success');
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Erro ao <strong>cadastrar</strong> o Outdoor. Por favor, tente novamente.'), 'flash/error');
				}
			}
		}
		$companies = $this->Outdoor->Company->find('list');
		$plans = $this->Outdoor->Plan->find('list');
		$segments = $this->Outdoor->Segment->find('list');
		$this->set(compact('companies', 'plans', 'segments'));

	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->title = 'Gerencimento do Outdoor';
    $this->description = 'Crie, edite ou remova qualquer banner no site.';
		$this->Outdoor->id = $id;
		if (!$this->Outdoor->exists($id)) {
			throw new NotFoundException(__('Não foi encontrado o registro para o ID especificado!'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if (!empty($this->request->data['Outdoor']['imagem']['name'])){
				// Monta o array de dados para o banco de dados
				$this->request->data['Outdoor']['ext'] = $this->Upload->get_extension($this->request->data['Outdoor']['imagem']);
				// Valida os dados da imagem com relação ao banco de dados

				$this->Outdoor->set($this->request->data);
				if( !$this->Outdoor->validates() ) {
					$result = 'error';
				}
				// Valida a imagem no componente
				$valida = $this->Upload->validate(
					$this->request->data['Outdoor']['imagem'],
					array('image/*')
				);
				if(!$valida){
					$result = 'error';
				}

				if($valida){
					// Monta o array de opções para o Download
					// ATENÇÃO !!! NÂO é necessário informar o diretório img
					// O diretório img já é definido em path_root
					// Caso queira que a imagem fique em outro diretório
					// informar path_root diferente: options['path_root'] = 'nove/endereco/para/imagem'
					$options = array(
						'name'    => $this->Outdoor->id,
						'dir'     => 'Outdoor',
					);
					$upload = $this->Upload->image($this->request->data['Outdoor']['imagem'], $options);
				}
			}
			if ($this->Outdoor->save($this->request->data)) {
				$this->Session->setFlash(__('O Outdoor <strong>editado</strong> com sucesso!'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Não foi possivel <strong>editar</strong> o Outdoor. Por favor, tente novamente.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Outdoor.' . $this->Outdoor->primaryKey => $id));
			$this->request->data = $this->Outdoor->find('first', $options);
		}
		$companies = $this->Outdoor->Company->find('list');
		$plans = $this->Outdoor->Plan->find('list');
		$segments = $this->Outdoor->Segment->find('list');
		$this->set(compact('companies', 'plans', 'segments'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Outdoor->id = $id;
		if (!$this->Outdoor->exists()) {
			throw new NotFoundException(__('Invalid outdoor'));
		}
		if ($this->Outdoor->delete()) {
			$this->Session->setFlash(__('Outdoor <b>deletado</b> com sucesso!'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Não foi possível <b>deletado</b> o outdoor'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
