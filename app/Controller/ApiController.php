<?php

App::uses('AppController', 'Controller');


class ApiController extends AppController {

  public $components = array('RequestHandler');

  public $uses = ['Company', 'Segment', 'Outdoor'];

  /**
   * companies method
   * /api/company/{id_company}.json
   * @return json/xml
   */
  public function company($id = NULL) {
    $this->Company->recursive = 1;
    $conditions = [
      'Company.active' => 0,
      'Company.' . $this->Company->primaryKey => $id
    ];
    $company = $this->Company->find('first', ['conditions'=> $conditions] );
    $this->set(array(
     'company' => $company,
     '_serialize' => array('company')
   ));
  }


  /**
   * segment method
   * /api/segments.json ou /api/segments/{id_segmento}.json
   * @return json/xml
   */
  public function segments($id = NULL) {
     $this->Segment->recursive = 0;
     $conditions = [
       'Segment.active' => 0,
     ];
     $format = 'all';
     if ($id) {
       $conditions['Segment.' . $this->Company->primaryKey] = $id;
       $format = 'first';
       $this->Segment->recursive = 2;
     }
     $segments = $this->Segment->find($format, ['conditions'=> $conditions, 'order'=> 'Segment.title ASC'] );
     $this->set(array(
      'segments' => $segments,
      '_serialize' => array('segments')
    ));
  }

  /**
   * busca method
   * /api/search/{string}/id_descrict.json ou /api/search/{string}.json
   * @return void
   */
  public function search() {
		$this->Company->recursive = 0;
		$typed_text = false;
		$filter = array( 'Company.active'=> 0 );

		$this->paginate = array(
			'order' => array('Company.title' => 'ASC'),
			'limit' => 50
		);
		if( isset($this->request->query['s']) ) {
			$this->redirect( array('action'=>'search', $this->request->query['s'], $this->request->query['bairro'] ) );
		}
		if( isset($this->request->params['pass'][0]) ){
			if( isset($this->request->params['pass'][1])){
				$filter['Company.district_id'] = $this->request->params['pass'][1];
			}
			$typed_text = $this->request->params['pass'][0];
			$keywords = explode(' ', $this->request->params['pass'][0]);
			foreach( $keywords as $keyword ){
				$keyword = trim($keyword);
				if( strlen( $keyword ) > 3 ){
					$filter['or'][] = array('Company.title LIKE' => "%{$keyword}%");
					$filter['or'][] = array('Company.description LIKE' => "%{$keyword}%");
				}
			}
		}
		$companies = $this->paginate($filter);
    $this->set(array(
     'companies' => $companies,
     '_serialize' => array('companies')
   ));
	}

  /**
   * companies method
   * /api/outdoors.json
   * @return json/xml
   */
  public function outdoors() {
    $this->Outdoor->recursive = 0;
    $conditions = [
      'Outdoor.active' => 0,
      'Outdoor.position'=> 1
    ];
    $outdoors = $this->Outdoor->find('all', ['conditions'=> $conditions] );
    $this->set(array(
     'outdoors' => $outdoors,
     '_serialize' => array('outdoors')
   ));
  }

}
