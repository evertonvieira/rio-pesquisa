<?php
App::uses('AppController', 'Controller');
/**
 * Payments Controller
 *
 * @property Payment $Payment
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class PaymentsController extends AppController {
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->title = 'Pagamentos';
    $this->description = 'Veja, edite e adicione os pagamentos dos clientes.';
		$this->Payment->recursive = 0;
		$this->set('payments', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->title = 'Pagamentos';
    $this->description = 'Veja, edite e adicione os pagamentos dos clientes.';
		if (!$this->Payment->exists($id)) {
			throw new NotFoundException(__('Invalid payment'));
		}
		$options = array('conditions' => array('Payment.' . $this->Payment->primaryKey => $id));
		$this->set('payment', $this->Payment->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->title = 'Pagamentos';
    $this->description = 'Veja, edite e adicione os pagamentos dos clientes.';
		if ($this->request->is('post')) {
			$this->Payment->create();
			if ($this->Payment->save($this->request->data)) {
				$this->Session->setFlash(__('Registro de pagamento <b>cadastrado</b> com sucesso!'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro de pagamento não pôde ser <b>cadastrado</b>. Por favor, verifique os campos e tente novamente.'), 'flash/error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->title = 'Pagamentos';
    $this->description = 'Veja, edite e adicione os pagamentos dos clientes.';
		$this->Payment->id = $id;
		if (!$this->Payment->exists($id)) {
			throw new NotFoundException(__('Invalid payment'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Payment->save($this->request->data)) {
				$this->Session->setFlash(__('Dados de pagamento <b>editado</b> com sucesso!'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro de pagamento não pôde ser <b>editado</b>. Por favor, verifique os campos e tente novamente.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Payment.' . $this->Payment->primaryKey => $id));
			$this->request->data = $this->Payment->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Payment->id = $id;
		if (!$this->Payment->exists()) {
			throw new NotFoundException(__('Invalid payment'));
		}
		if ($this->Payment->delete()) {
			$this->Session->setFlash(__('Registro de pagamento <b>deletado</b> com sucesso!'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Erro ao <b>deletar</b> o registro de pagamento'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_getPayments($type, $month = NULL, $year = NULL,  $status = 1){
		$this->autoRender = false;
		$this->Payment->recursive = 0;
		$year = ($year == NULL ) ? date("Y") : $year;
		if ($type == 'all') {

			$debite = $this->Payment->find('all', [
				'conditions'=> [
					'MONTH(Payment.data_pago)' => $month,
					'YEAR(Payment.data_pago)' => $year,
					'Payment.status'=> $status,
					'Payment.type'=> 1
				]
			]);

			$credite = $this->Payment->find('all', [
				'conditions'=> [
					'MONTH(Payment.data_pago)' => $month,
					'YEAR(Payment.data_pago)' => $year,
					'Payment.status'=> $status,
					'Payment.type'=> 0
				]
			]);

			return (float) $this->sumPayments($credite) - $this->sumPayments($debite);

		}else{
			$conditions = [
				'Payment.type'=> $type,
				'MONTH(Payment.data_pago)' => $month,
				'YEAR(Payment.data_pago)' => $year,
				'Payment.status'=> $status
			];
		}
		$payments = $this->Payment->find('all', [
			'conditions'=> $conditions
		]);

		return (float) $this->sumPayments($payments);
	}

	public function sumPayments($payments = []){
		(float) $sum = 0;
		foreach ($payments as $payment) {
			$sum += $payment['Payment']['value'];
		}
		return $sum;
	}

}
