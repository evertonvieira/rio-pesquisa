<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->title = "Usuários";
    $this->description = "Gerencie todos os usuários do sistema.";
		$this->User->recursive = 1;
		$this->set('users', $this->User->find("all"));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->title = "Usuários";
		$this->description = "Veja todos os dados do usuário.";
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		$this->title = "Usuários";
		$this->description = "Gerencie todos os usuários do sistema.";
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('usuário <b>salvo</b> com sucesso!'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao <b>salvar</b> o usuário. Por favor, tente novamente.'), 'flash/error');
			}
		}
	}

	public function admin_edit($id = null) {
		$this->title = "Usuários";
		$this->description = "Gerencie todos os usuários do sistema.";
    $this->User->id = $id;
    if (!$this->User->exists()) {
      throw new NotFoundException(__('Invalid user'));
    }
    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->User->save($this->request->data)) {
        $this->Session->setFlash(__('usuário <b>editado</b> com sucesso!'), 'flash/success');
        $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('Erro ao <b>editar</b> o usuário. Por favor, tente novamente.'), 'flash/error');
      }
    } else {
      $this->request->data = $this->User->read(null, $id);
    }
  }

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('usuário <b>deletado</b> com sucesso!'), 'flash/success');
		} else {
    	$this->Session->setFlash(__('Erro ao <b>deletar</b> o usuário. Por favor, tente novamente.'), 'flash/error');
		}
		return $this->redirect(array('action' => 'index'));
	}


	//autenticação
	public function admin_login() {
		$this->title = "Área Restrita";
		$this->description = "acesso somente com credenciais.";
		$this->layout ="layout_login";
		if ($this->Auth->login()) {
      return $this->redirect($this->Auth->redirect());
    }
		if ($this->request->is('post') || $this->request->is('put')) {
	    if ($this->Auth->login()) {
	      $this->redirect($this->Auth->redirect());
	    } else {
				//$this->Session->setFlash(__('Invalid username or password, try again'));
	    	$this->Session->setFlash(__('Dados digitados não conferem. Por favor, tente novamente.'), 'flash/error');
	    }
		}
	}

	public function logout(){
		$this->Session->destroy();
    $this->redirect( $this->Auth->logout() );
	}

}
