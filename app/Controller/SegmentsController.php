<?php
App::uses('AppController', 'Controller');
/**
 * Segments Controller
 *
 * @property Segment $Segment
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class SegmentsController extends AppController {

/**
 * loadAllSegments method
 * @string $format
 * @return void
 */

	public function	loadAllSegments( $format = 'list' ){
		$this->recursive = 0;
		$this->autoRender = false;
		$segments = $this->Segment->find($format, ['conditions'=>['Segment.active'=> 0],'fields'=>['Segment.slug', 'Segment.title']]);
		return $segments;
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($slug = null) {
		$segment = $seo = $this->Segment->findBySlug($slug);
		$this->set(compact('segment','seo'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->title = 'Segmentos';
    $this->description = 'Gerencie todos so segmentos das empresas/comércios.';
		$this->Segment->recursive = 0;
		$this->set('segments', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->title = 'Segmentos';
    $this->description = 'Gerencie todos so segmentos das empresas/comércios.';
		if (!$this->Segment->exists($id)) {
			throw new NotFoundException(__('Invalid segment'));
		}
		$options = array('conditions' => array('Segment.' . $this->Segment->primaryKey => $id));
		$this->set('segment', $this->Segment->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->title = 'Segmentos';
    $this->description = 'Gerencie todos so segmentos das empresas/comércios.';
		if ($this->request->is('post')) {
			$this->Segment->create();
			if ($this->Segment->save($this->request->data)) {
				$this->Session->setFlash(__('Segmento <b>cadastrado</b> cadastrado com sucesso!'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao <b>cadastrar</b> o segmento. Por favor, verifique os campos e tente novamente.'), 'flash/error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->title = 'Segmentos';
    $this->description = 'Gerencie todos so segmentos das empresas/comércios.';
		$this->Segment->id = $id;
		if (!$this->Segment->exists($id)) {
			throw new NotFoundException(__('Invalid segment'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Segment->save($this->request->data)) {
				$this->Session->setFlash(__('Dados do segmento <b>editado</b> com sucesso!'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Houve um erro ao <b>editar</b> o segmento. Por favor, verifique os campos e tente novamente.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Segment.' . $this->Segment->primaryKey => $id));
			$this->request->data = $this->Segment->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Segment->id = $id;
		if (!$this->Segment->exists()) {
			throw new NotFoundException(__('Invalid segment'));
		}
		if ($this->Segment->delete()) {
			$this->Session->setFlash(__('Segmento <b>deletado</b> com sucesso!'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Houve um erro ao <b>deletar</b> o segmento. Por favor, tente novamente.'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
