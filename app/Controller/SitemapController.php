<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 */
class SitemapController extends AppController {
  var $uses = array('Segment', 'Page', 'Company');
  var $components = array('RequestHandler');
  /**
   * index method
   *
   * @return void
   */
	public function index(){
		$sitemaps = array(
      'common',
      'pages',
      'segments',
      'companies',
    );
    $this->set(compact('sitemaps'));
  }
	public function common(){
		$statics = array(
      '',
    );
    $this->set(compact('statics'));
  }
	public function pages(){
		$this->Page->recursive = 0;
    $pages = $this->Page->find('list');
    $this->set(compact('pages'));
  }

	public function segments(){
		$this->Segment->recursive = 0;
    $segments = $this->Segment->find('list',
      [
        'conditions'=>[
          'Segment.active' => 0
        ],
        'fields'=>'slug'
      ]
    );
    $this->set(compact('segments'));
  }

	public function companies(){
		$this->Company->recursive = 0;
    $companies = $this->Company->find('all',
     [
      'conditions'=>[
        'Company.active' => 0,
        'NOT' => ['Company.plan_id' => 4 ]
      ],
    ]);
    $this->set(compact('companies'));
  }
}
