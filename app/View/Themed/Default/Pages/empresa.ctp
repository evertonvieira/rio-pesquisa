<article class="Page">
	<header class="page-header">
		<div class="container">
			<h1><?=$page['Page']['title'];?></h1>
		</div>
	</header>
	<main class="container" id="body-content">
			<?=$page['Page']['body'];?>
	</main>
</article>
