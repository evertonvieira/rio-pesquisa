<article class="Page">
	<header class="page-header">
		<div class="container">
			<h1><?=$page['Page']['title'];?></h1>
		</div>
	</header>
	<main class="container">
			<?=$page['Page']['body'];?>
	</main>
	<?php echo $this->element("tinymce"); ?>
	<div  id="body-content" class="container">
	  <div class="row">
	    <div class="col-md-12">
	      <div class="flash">
	        <?php echo $this->Flash->render('flash'); ?>
	      </div>
	    	<?php echo $this->Form->create('Registration', array('controller'=>'registrations', 'action'=>'add', 'type' => 'file')); ?>
	        <div class="box-body">
	          <header class="page-header">
	            <h2>Dados Pessoais:</h2>
	          </header>
						<div class="form-group">
							<?php echo $this->Form->input('titular', array('label'=>'Nome do responsável:', 'class'=>'form-control input-lg', 'placeholder'=>'Joaquim Barbosa'));?>
						</div>
						<div class="form-group">
							<?php echo $this->Form->input('data_nascimento', array('label'=>'Data de nascimento:', 'type'=> 'text', 'class'=>'form-control', 'placeholder'=>'00/00/0000'));?>
						</div>
	          <div class="form-group">
	            <label>Gênero:</label>
	            <?php
	            echo $this->Form->input("genero", array(
	              'type' => 'select',
	              'label'=>false,
	              'options' => array(0=>'Masculino', 1=>'Feminino'),
	              'escape'=>false,
	              'class'=>'form-control'
	            ));
	            ?>
	          </div>
						<div class="form-group">
							<?php echo $this->Form->input('email', array('label'=>'Email para contato:', 'type'=> 'email','class'=>'form-control', 'placeholder'=>'example@example.com'));?>
						</div>
						<div class="form-group">
							<?php echo $this->Form->input('telefone', array('label'=>'Telefone para contato:', 'class'=>'form-control', 'placeholder'=>'(00) 00000-0000'));?>
						</div>

	          <div class="dados_comercio">
	            <header class="page-header">
	              <h2>Dados de sua empresa:</h2>
	            </header>
	  					<div class="form-group">
	  						<?php echo $this->Form->input('nome_comercio', array('label'=>'Nome da empresa:', 'class'=>'form-control', 'placeholder'=>'ex.: MJM - Materiais de Construção'));?>
	  					</div>
	  					<div class="form-group">
	  						<?php echo $this->Form->input('endereco', array('label'=>'Endereco:', 'class'=>'form-control', 'placeholder'=>'ex.: Avenida Central n°1000, Jardim Madalena, Rio de Janeiro'));?>
	  					</div>
	  					<div class="form-group">
	  						<?php echo $this->Form->input('telefone_contato', array('label'=>'Telefone de contato:', 'class'=>'form-control', 'placeholder'=>'(00)00000-0000'));?>
	  					</div>
	  					<div class="form-group">
	              <?php echo $this->Form->input('whatsapp', array('label'=>'Whatsapp:', 'class'=>'form-control', 'placeholder'=>'(00) 00000-0000'));?>
	  					</div>
	  					<div class="form-group">
	  						<?php echo $this->Form->input('email_contato', array('label'=>'E-mail:','type'=>'email', 'class'=>'form-control', 'placeholder'=>'example@example.com'));?>
	  					</div>
	  					<div class="form-group">
	  						<?php echo $this->Form->input('site', array('label'=>'Site:', 'class'=>'form-control', 'placeholder'=>'http://meusite.com.br'));?>
	  					</div>
	  					<div class="form-group">
	  						<?php echo $this->Form->input('segmento', array('label'=>'Segmento de atuação da empresa:', 'class'=>'form-control', 'placeholder'=>'ex.: Pizzaria e Delivery, Casa e Construção, Saúde e Bem Estar, etc.'));?>
	  					</div>
	  					<div class="form-group">
	  						<?php echo $this->Form->input('imagem', array('label'=>'Imagem(logo da empresa):', 'class'=>'form-control', 'type'=>'file'));?>
	  					</div>
	  					<div class="form-group">
	  						<?php echo $this->Form->input('description', array('label'=>'Descrição da empresa:', 'type'=>'textarea', 'class'=>'editor form-control', 'rows'=>12));?>
	  					</div>
	          </div>
	        </div>
					<div class="box-footer">
						<?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-lg', 'escape' => false)); ?>
					</div>
	      <?php echo $this->Form->end(); ?>
	    </div>
	  </div>
	</div>

</article>
