<article class="Page">
	<header class="page-header">
		<div class="container">
			<h1><?=$page['Page']['title'];?></h1>
		</div>
	</header>
	<main class="container" id="body-content">
		<?php echo $this->Flash->render('flash'); ?>
		<section id="sidebar-info" class="contato-info col-md-4">
			<div class="page-header">
				<h2>INFORMAÇÕES</h2>
			</div>
			<p><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> (21) 3116-6710 / (21) 99717-6982</p>
			<p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> contato@pesquisacom.com</p>
			<p><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> De seg à sex das 09:00h às 19:00h</p>
			<div class="box-social">
				<?php echo $this->Html->link($this->Html->image('icon-footer-face.png', array('alt' => 'Facebook',)), "https://www.facebook.com/pesquisacom", array('target'=>'_blank','escape' => false));?>
        <?php echo $this->Html->link($this->Html->image('icon-footer-insta.png', array('alt' => 'Instagram',)), "https://www.instagram.com/pesquisacom/", array('target'=>'_blank', 'escape' => false));?>
	    </div>
		</section>
		<article class="col-md-8">
			<div class="page-header">
				<h2>FORMULÁRIO DE CONTATO</h2>
			</div>
			<?php echo $this->Form->create('Contact', array('controller'=>'contacts', 'action'=>'index', 'role' => 'form', 'class'=>'form-contacts')); ?>
				<div class="row">
					<div class="form-group col-md-12">
						<?php echo $this->Form->input('name', array('label'=>false, 'class'=>'form-control input-lg', 'placeholder'=>'Nome:'));?>
					</div>
					<div class="form-group col-md-12">
						<?php echo $this->Form->input('email', array('label'=>false, 'class'=>'form-control input-lg', 'placeholder'=>'E-mail:'));?>
					</div>
					<div class="form-group col-md-6">
						<?php echo $this->Form->input('phone', array('label'=>false, 'class'=>'form-control input-lg', 'id'=>'phone', 'placeholder'=>'Telefone:'));?>
					</div>
					<div class="form-group col-md-6">
						<?php echo $this->Form->input('whatsapp', array('label'=>false, 'class'=>'form-control input-lg', 'id'=>'whatsapp', 'placeholder'=>'Whatsapp:'));?>
					</div>
					<div class="form-group col-md-12">
						<?php echo $this->Form->input('subject', array('label'=>false, 'class'=>'form-control input-lg', 'placeholder'=>'Assunto:'));?>
					</div>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('message', array('label'=>false, 'placeholder'=>'Mensagem:', 'class'=>'form-control input-lg', 'type'=>'textarea', 'rows'=>5));?>
				</div>
				<div class="pull-left">
					<?php echo $this->Form->button('Enviar', array('type' => 'submit', 'class' => 'btn btn-lg', 'escape' => false)); ?>
				</div>
			<?php echo $this->Form->end(); ?>
		</article>
	</main>
</article>
