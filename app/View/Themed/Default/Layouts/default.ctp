<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<?php echo $this->Seo->metaTags( isset ($seo ) ? $seo : null ); ?>
		<?php echo $this->Html->charset(); ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300i,400,400i,700i" rel="stylesheet">
		<?php
			echo $this->Html->meta('icon');
			echo $this->Html->css(array(
        'assets.min',
			));
			echo $this->Html->script(array(
				'assets.min',
			));
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-5224090247986924",
        enable_page_level_ads: true
      });
    </script>
    <meta name="google-site-verification" content="XZZnB8SWZa8gSkRCmeeFH5eAY3XeAw2-azXqGYe5oww" />
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-96168756-1', 'auto');
      ga('send', 'pageview');
    </script>
  </head>
	<body>
    <?php echo $this->element("header"); ?>
		<?php echo $this->Flash->render(); ?>
		<?php echo $this->fetch('content'); ?>
    <?php echo $this->element("newsletter"); ?>
    <?php echo $this->element("footer"); ?>
		<!-- Your Page Content Here -->
  </body>
</html>
