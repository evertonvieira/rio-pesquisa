$(document).ready( () => {

  //
  $(".SelectSegment").select2({
    placeholder: "Selecione um segmento",
    theme: "bootstrap",
    width: null,
    allowClear: true,
    containerCssClass: ':all:'
  });

});
