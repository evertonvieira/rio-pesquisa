$(document).ready( () => {

  $("#phone, #whatsapp").mask("(99) 9999-9999?9");

  //slideshow segment
  $('.bxslider').bxSlider({
    mode:'fade',
    auto: true,
    speed: 500,
    autoDelay: 200,
    autoControls: false,
    adaptiveHeight: true,
    infiniteLoop: true,
    hideControlOnEnd: true
  });
  $('.VerticalSlide').bxSlider({
     mode: 'horizontal',
     auto: true,
     minSlides: 1,
     slideMargin: 10
   });
  $('[data-toggle="tooltip"]').tooltip()

  //carosel segments
  $(".owl-carousel").owlCarousel({
    items: 10,
    nav: true,
    responsive:{
        0:{
          items: 2
        },
        600:{
          items: 5
        },
        1000:{
          items: 8
        }
    }
  });

  //
  $(".SelectTag").select2({
    placeholder: "Selecione um bairro",
    theme: "bootstrap",
    width: null,
    allowClear: true,
    containerCssClass: ':all:'
  });

  $("#btn_search").click( () => {
    if ($("#form_search").val() == "") {
      alert("Digite um termo a ser pesquisado.");
      return false;
    }
  });

});
