<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us17.list-manage.com","uuid":"6f48807b2f31acd709c141047","lid":"a01bf7d758"}) })</script>
<section class="nav-line">
  <div class="container">
    <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
  	<span>(21) 3116-6710 / Whatsapp: (21) 99717-6982</span>
  	<span class="line-vertical">|</span>
  	<span>Siga-nos:</span>
    <?php echo $this->Html->link($this->Html->image('icon-face-top.png', array('alt' => 'Facebook',)), "https://www.facebook.com/pesquisacom", array('target'=>'_blank','escape' => false));?>
    <?php echo $this->Html->link($this->Html->image('icon-insta-top.png', array('alt' => 'Instagram',)), "https://www.instagram.com/pesquisacom/", array('target'=>'_blank', 'escape' => false));?>
  </div>
</section>
<header id="header"> 
  <div class="container">
    <div class="row">
      <section class="col-md-8 col-xs-12">
        <h1>
          <?php echo $this->Html->link("Pesquisa Com.com", array('plugin'=>false, 'controller' => 'home', 'action'=>'/'), array('title'=>'Pesquisa Com.com', 'class'=>'logo-mobile','escape' => false));?>
        </h1>
        <span class="line-vertical-top"></span>
      	<p>Todos os Comércios que <wbr>você precisa em um só lugar!</wbr></p>
      </section>
      <section id="menu" class="col-md-4 col-xs-12">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li class="<?= ($this->name == "Home") ? "active": "" ?>">
              <?php echo $this->Html->link("Home", array('plugin'=>false, 'controller' => 'home', 'action'=>'home'), array('escape' => false));?>
            </li>
            <li class="<?= ($this->params['slug'] == "quem-somos") ? "active": "" ?>">
              <?php echo $this->Html->link("Quem Somos", array('plugin'=>false, 'controller' => 'pages', 'action'=>'view', 'slug'=>'quem-somos'), array('escape' => false));?>
            </li>
            <li class="<?= ($this->params['slug'] == "fale-conosco") ? "active": "" ?>">
              <?php echo $this->Html->link("Contato", array('plugin'=>false, 'controller' => 'pages', 'action'=>'view', 'slug'=>'fale-conosco'), array('escape' => false));?>
            </li>
            <li class="<?= ($this->params['slug'] == "anuncie") ? "active": "" ?>">
              <?php echo $this->Html->link("Anuncie!", array('plugin'=>false, 'controller' => 'pages', 'action'=>'view', 'slug'=>'anuncie'), array('escape' => false));?>
            </li>
          </ul>
        </nav>
      </section>
    </div>
  </div>
</header>
<section id="segments">
  <nav class="container owl-carousel owl-theme">
    <?php
      $segments = $this->RequestAction("Segments/loadAllSegments/all");
      foreach ($segments as $key => $segment){
        $active = ($this->params->slug == $segment['Segment']['slug'] || $this->params->action == $segment['Segment']['slug'] || $this->params['segment'] == $segment['Segment']['slug']) ? "active": "";
        echo $this->Html->link("<h2>{$segment['Segment']['title']}</h2>", array('plugin'=>false, 'controller'=> 'segments', 'action'=>'view', 'slug'=> $segment['Segment']['slug']), array('class'=>"col-xs-6 col-sm-2 {$active} {$segment['Segment']['slug']} placeholder",'escape' => false));
      }
    ?>
  </nav>
</section>
<?php if ($this->name != "Pages"): ?>
  <section id="search">
  	<div class="container">
  		<div class="row">
        <?php
          echo $this->Form->create('companies', array('class' => '','type' => 'get', 'action' => '/search/'));
        ?>
  				<div class="form-group col-md-7">
  					<?php echo $this->Form->input('s', array('label'=>false,  'id'=>'form_search', 'class'=>'form-control input-lg', 'placeholder'=>'O que você procura?'));?>
  				</div>
          <div class="col-md-5">
            <div class="input-group">
              <?php
                $districts = $this->RequestAction("Districts/loadDistricts");
                echo $this->Form->input('bairro', array('label'=>false, 'class'=>'SelectTag form-control input-lg',  'options'=> [''=>'Selecione um bairro', $districts], 'type'=>'select'));
              ?>
              <div class="input-group-btn">
                <?php echo $this->Form->button('<span class="glyphicon glyphicon-search" aria-hidden="true"></span>', array('type' => 'submit', 'id'=>'btn_search','class' => 'btn btn-default btn-lg', 'escape' => false)); ?>
              </div>
            </div>
          </div>
  			<?php echo $this->Form->end(); ?>
  		</div>
  	</div>
  </section>
<?php endif; ?>
