<section class="slideshow">
  <ul class="bxslider">
    <?php
      $outdoors = $this->RequestAction("outdoors/getOutdoors/{$slugSegment}/{$plan_id}/{$position}");
      foreach ($outdoors as $key => $outdoor):?>
      <li>
        <?php
        if ($outdoor['Outdoor']['link']) {
          echo $this->Html->link($this->Html->image("Outdoor/{$outdoor['Outdoor']['id']}.{$outdoor['Outdoor']['ext']}", array('alt' => '',)), $outdoor['Outdoor']['link'], array('target'=>'_blank', 'escape' => false));
        }else{
          echo $this->Html->link($this->Html->image("Outdoor/{$outdoor['Outdoor']['id']}.{$outdoor['Outdoor']['ext']}", ['class'=>'img-responsive', 'alt'=>$outdoor['Outdoor']['title']] ), array('plugin'=>false, 'controller' => 'companies', 'action'=>'view', 'segment'=>$outdoor['Segment']['slug'], 'slug'=>$outdoor['Company']['slug']), array('escape' => false));
        }
        ?>
      </li>
    <?php endforeach; ?>
  </ul>
</section>
