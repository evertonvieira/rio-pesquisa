<footer id="footer">
  <section class="container">
    <div class="row">
      <div class="col-md-4 box">
        <h1>Entre em Contato</h1>
        <p><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> (21) 3116-6710 / Whatsapp: (21) 99717-6982</p>
        <p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> contato@pesquisacom.com</p>
        <p><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> De seg à sex das 09:00h às 19:00h</p>
      </div>
      <div class="col-md-4 box">
        <h1>Curta Nossa Página</h1>
        <div class="fb-like-box" data-href="https://www.facebook.com/pesquisacom/" data-width="300" data-adapt-container-width="true" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
      </div>
      <div class="col-md-4 box">
        <h1>Siga-nos nas Redes Sociais</h1>
        <p>Fique por dentro das novidades, interaja com a comunidade, receba dicas interessantes diariamente, ou simplesmente mostre ao mundo que você faz parte desse projeto sensacional!</p>
        <?php echo $this->Html->link($this->Html->image('icon-footer-face.png', array('alt' => 'Facebook',)), "https://www.facebook.com/pesquisacom", array('target'=>'_blank','escape' => false));?>
        <?php echo $this->Html->link($this->Html->image('icon-footer-insta.png', array('alt' => 'Instagram',)), "https://www.instagram.com/pesquisacom/", array('target'=>'_blank', 'escape' => false));?>
      </div>
    </div>
  </section>
</footer>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=164853720575055";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section id="copyright">
  <div class="container">
    <p> &copy; 2017 - Plataforma de anúncios de bairro online - Todos os direitos reservados.</p>
  </div><!--Copyright-->
</section>
