<section class="container">
  <div class="row boxes" id="area-bronze">
    <div class="col-md-8" id="bronzeAnuncios">
      <?php
        $slugSegment = ($this->params->slug) ? $this->params->slug : "NULL";
        $companiesBronze = $this->requestAction("companies/getCompanies/{$slugSegment}/4/all/12");
        foreach ($companiesBronze as $key => $bronze):
      ?>
        <article class="col-md-3 col-xs-6">
          <div class="box">
            <h4 data-toggle="tooltip" data-placement="top" title="<?= $bronze['Company']['title']; ?>">
              <?= $this->Formatacao->LimitaCaracter($bronze['Company']['title'], 40); ?>
            </h4>
            <address>
              <p>
                <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                <?php echo $bronze['District']['title']; ?>
              </p>
              <?php if ($bronze['Company']['telefone']): ?>
                <p>
                  <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> <?= $bronze['Company']['telefone']; ?>
                </p>
              <?php endif; ?>
            </address>
          </div>
        </article>
      <?php endforeach; ?>
    </div>
    <div class="col-md-4" style="overflow:hidden;">
      <!-- bloco-bronze -->
     <ins class="adsbygoogle"
          style="display:block"
          data-ad-client="ca-pub-5954774589011046"
          data-ad-slot="9330498370"
          data-ad-format="auto"></ins>
     <script>
     (adsbygoogle = window.adsbygoogle || []).push({});
     </script>
    </div>
  </div>
</section>
<hr class="divided" />
