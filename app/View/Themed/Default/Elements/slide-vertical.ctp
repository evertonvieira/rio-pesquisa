<div class="col-md-4" id="slideVertical">
  <div class="VerticalSlide">
    <?php
      $outdoors = $this->RequestAction("outdoors/getOutdoors/{$slugSegment}/{$plan_id}/{$position}");
      foreach ($outdoors as $key => $outdoor):?>
      <div class="slide">
        <?php
          if ($outdoor['Outdoor']['link']) {
            echo $this->Html->link($this->Html->image("Outdoor/{$outdoor['Outdoor']['id']}.{$outdoor['Outdoor']['ext']}", array('alt' => '',)), $outdoor['Outdoor']['link'], array('target'=>'_blank', 'escape' => false));
          }else{
            echo $this->Html->link($this->Html->image("Outdoor/{$outdoor['Outdoor']['id']}.{$outdoor['Outdoor']['ext']}", ['class'=>'img-responsive', 'alt'=>$outdoor['Outdoor']['title']] ), array('plugin'=>false, 'controller' => 'companies', 'action'=>'view', 'segment'=>$outdoor['Segment']['slug'], 'slug'=>$outdoor['Company']['slug']), array('escape' => false));
          }
        ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>
