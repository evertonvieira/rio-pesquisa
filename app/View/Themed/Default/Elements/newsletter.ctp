<section id="newsletter">
  <div class="container">
    <h2>Receba semanalmente e-mails com promoções e novos comércios cadastrados em nossa plataforma.</h2>
    <form action="//pesquisacom.us15.list-manage.com/subscribe/post?u=4d739ff15ab648b7d76128276&amp;id=bb15033efa" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="input-group">
            <input type="email" value="" name="EMAIL" class="form-control input-lg" id="mce-EMAIL" placeholder="email@exemplo.com.br" required>
            <div class="input-group-btn">
              <input type="submit" value="Assinar" name="subscribe" id="mc-embedded-subscribe" class="btn btn-default btn-lg">
    				</div>
    			</div>
    		</div>
    	</div>
    </form>
  </div>
</section>
