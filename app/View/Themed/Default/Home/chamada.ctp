<script type="text/javascript">
	$(document).ready(function(){
		$('.btnSearch').click(function (e) {
			let url = $('.SelectSegment').val();
			let action;
			if (url == "") {
				 action = "<?php echo Router::url(array('controller'=>'/home'));?>";
			}else{
				action = "<?php echo Router::url(array('controller'=>'segmentos'));?>/"+ url + '/';
			}
			window.location = action;
		});
	});
</script>

<div class="site-wrapper-inner">
	<div class="cover-container">
		<div class="masthead clearfix">
			<div class="inner">
				<header>
					<h1><?php echo $this->Html->image("logo_home.png", ['class'=>'img-responsive logo']); ?></h1>
				</header>
			</div>
		</div>

		<div class="inner cover">
			<h2 class="cover-heading">Precisa encontrar algum comércio? Faça sua busca em nossa plataforma.</h2>
			<p class="lead">Todas as empresas e comércios cadastrados em nossa plataforma, são todos confiavéis e fazem um ótimo atendimento.</p>
		</div>
		<div class="FormSearchHome">
	  	<div class="box-body">
		    <div class="form-group col-md-12">
		      <?php
						$segments = $this->RequestAction("Segments/loadAllSegments");
						echo $this->Form->input('segment', array('label'=>false, 'class'=>'input-lg SelectSegment form-control', 'options'=> [''=>'Selecione um segmento', $segments], 'placeholder'=>"testt test", 'type'=>'select'));
					?>
		    </div>
		  </div>
		  <div class="box-footer">
		    <?php echo $this->Form->button('<i class="glyphicon glyphicon-search"></i> pesquisar', array('type' => 'submit', 'class' => 'btn btn-large btn-success btn-lg btnSearch', 'escape' => false)); ?>
		  </div>
		</div>
		<div class="mastfoot">
			<div class="inner">
				<p>Copyright &copy; 2017 <a href="http://pesquisacom.com">PesquisaCom.com</a>. Todos os diretos reservados.</p>
			</div>
		</div>
	</div>
</div>
