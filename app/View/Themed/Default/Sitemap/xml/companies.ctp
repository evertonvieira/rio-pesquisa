<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <?php foreach($companies as $company): ?>
  <url>
    <loc><?=Router::url('/', true);?>segmentos/<?=$company['Segment']['slug'];?>/<?= $company['Company']['slug'];?></loc>
    <changefreq>daily</changefreq>
    <priority>0.80</priority>
    <lastmod><?=date('c',time());?></lastmod>
  </url>
  <?php endforeach;?>
</urlset>
