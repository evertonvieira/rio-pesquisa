<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <?php foreach($sitemaps as $sitemap):?>
  <sitemap>
    <loc><?=Router::url('/', true);?>sitemap/<?=$sitemap;?>.xml</loc>
    <lastmod><?=date('c',time());?></lastmod>
  </sitemap>
  <?php endforeach;?>
</sitemapindex>

