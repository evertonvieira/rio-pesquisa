<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <?php foreach($pages as $page):?>
  <url>
    <loc><?=Router::url('/', true);?>pagina/<?=$page;?></loc>
    <changefreq>daily</changefreq>
    <priority>0.80</priority>
    <lastmod><?=date('c',time());?></lastmod>
  </url>
  <?php endforeach;?>
</urlset>