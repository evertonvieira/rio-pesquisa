<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <?php foreach($segments as $id => $segment):?>
  <url>
    <loc><?=Router::url('/', true);?>segmentos/<?=$segment;?></loc>
    <changefreq>daily</changefreq>
    <priority>0.80</priority>
    <lastmod><?=date('c',time());?></lastmod>
  </url>
  <?php endforeach;?>
</urlset>
