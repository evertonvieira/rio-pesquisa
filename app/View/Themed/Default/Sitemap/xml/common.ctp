<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc><?=Router::url('/', true);?></loc>
    <changefreq>daily</changefreq>
    <priority>0.80</priority>
    <lastmod><?=date('c',time());?></lastmod>
  </url>
</urlset>