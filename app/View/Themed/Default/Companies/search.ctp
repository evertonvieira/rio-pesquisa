<hr class="divided" />
<div class="container">
  <?php if (isset($this->request->params['pass'])){ ?>
    Você pesquisou por: <b><i><?php echo $this->request->params['pass'][0];?></i></b>
  <?php } ?>
  <div>
  </div>
</div>

<hr class="divided" />

<?php if (count($companies) <= 0 ):  ?>
  <hr class="divided" />
  <section class="container">
    <h3>Não foi encontrado nenhum resultado para a sua pesquisa: <em><?php echo $this->request->params['pass'][0];?></em></h3>
  </section>
<?php endif; ?>
<?php if (!empty($companies['Gold']) ):  ?>
  <section class="container">
    <div class="row boxes" id="area-prata">
      <?php
        foreach ($companies['Gold'] as $key => $gold):
      ?>
        <article class="box col-xs-6 col-md-4">
          <div class="box-overlay">
            <figure class="image">
              <?php echo $this->Html->link($this->Html->image("Company/{$gold['Company']['id']}/{$gold['Company']['id']}.{$gold['Company']['ext']}", ['class'=>'img-responsive', 'alt'=>$gold['Company']['title']] ), array('plugin'=>false, 'controller'=> 'companies', 'action'=>'view', 'segment'=> $gold['Segment']['slug'], 'slug'=>$gold['Company']['slug']), array('escape' => false));?>
            </figure>
            <a title="<?= $gold['Segment']['title']; ?>" href="<?php echo Router::url(array('controller'=>'segments','action'=>'view', 'slug'=>$gold['Segment']['slug'], 'admin'=>false));?>">
              <span data-toggle="tooltip" data-placement="top" data-original-title="<?= $gold['Segment']['title']; ?>" class="icon-segment-title <?= $gold['Segment']['slug']; ?>">
                <h3><?= $gold['Segment']['title']; ?></h3>
              </span>
            </a>
          </div>
          <h2>
            <?php echo $this->Html->link($gold['Company']['title'], array('plugin'=>false, 'controller' => 'companies', 'action'=>'view', 'segment'=> $gold['Segment']['slug'], 'slug'=>$gold['Company']['slug'] ), array('escape' => false));?>
          </h2>
          <address>
            <p>
              <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?= $gold['Company']['rua']; ?>, n° <?= $gold['Company']['number']; ?>, <?= $gold['District']['title']; ?></h3>
            </p>
            <?php if ($gold['Company']['telefone']): ?>
              <p>
                <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> <?= $gold['Company']['telefone']; ?>
              </p>
            <?php endif; ?>
            <?php if ($gold['Company']['email']): ?>
              <p class="info_email">
                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> <?= $gold['Company']['email']; ?>
              </p>
            <?php endif; ?>
          </address>
        </article>
      <?php endforeach; ?>
      <article class="col-xs-6 col-md-4">
        <!-- bloco-gold -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-5954774589011046"
             data-ad-slot="9330498370"
             data-ad-format="auto"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </article>
    </div>
  </section>
  <hr class="divided" />
<?php endif; ?>
<?php if (!empty($companies['Premium']) ):  ?>
  <section class="container">
    <div class="row boxes" id="area-prata">
      <?php foreach ($companies['Premium'] as $key => $premium): ?>
        <article class="box col-xs-6 col-md-3">
          <div class="box-overlay">
            <figure class="image">
              <?php echo $this->Html->link($this->Html->image("Company/{$premium['Company']['id']}/{$premium['Company']['id']}.{$premium['Company']['ext']}", ['class'=>'img-responsive', 'alt'=>$premium['Company']['title']] ), array('plugin'=>false, 'controller'=> 'companies', 'action'=>'view', 'segment'=> $premium['Segment']['slug'], 'slug'=>$premium['Company']['slug']), array('escape' => false));?>
            </figure>
            <a title="<?= $premium['Segment']['title']; ?>" href="<?php echo Router::url(array('controller'=>'segments','action'=>'view', 'slug'=>$premium['Segment']['slug'], 'admin'=>false));?>">
              <span data-toggle="tooltip" data-placement="top" data-original-title="<?= $premium['Segment']['title']; ?>" class="icon-segment-title <?= $premium['Segment']['slug']; ?>">
                <h3><?= $premium['Segment']['title']; ?></h3>
              </span>
            </a>
          </div>
          <h2>
            <?php echo $this->Html->link($premium['Company']['title'], array('plugin'=>false, 'controller' => 'companies', 'action'=>'view', 'segment'=> $premium['Segment']['slug'], 'slug'=>$premium['Company']['slug'] ), array('escape' => false));?>
          </h2>
          <address>
            <p>
              <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?= $premium['Company']['rua']; ?>, n° <?= $premium['Company']['number']; ?>, <?= $premium['District']['title']; ?></h3>
            </p>
          </address>
        </article>
    <?php endforeach; ?>
      <article class="col-xs-6 col-md-12">
        <!-- bloco-prata -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-5954774589011046"
             data-ad-slot="9330498370"
             data-ad-format="auto"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </article>
    </div>
  </section>
  <hr class="divided" />
<?php endif; ?>

<?php if (!empty($companies['Bronze']) ):  ?>
  <section class="container">
    <div class="row boxes" id="area-bronze">
      <div class="col-md-8" id="bronzeAnuncios">
        <?php foreach ($companies['Bronze'] as $key => $bronze):
        ?>
          <article class="col-md-4 col-xs-6">
            <div class="box">
              <h4 data-toggle="tooltip" data-placement="top" title="<?= $bronze['Company']['title']; ?>"><?= $bronze['Company']['title']; ?></h4>
              <h5><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo $bronze['District']['title']; ?></h5>
            </div>
          </article>
        <?php endforeach; ?>
      </div>
      <div class="col-md-4" style="overflow:hidden;">
        <!-- bloco-bronze -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-5954774589011046"
             data-ad-slot="9330498370"
             data-ad-format="auto"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </div>
    </div>
  </section>
  <hr class="divided" />
<?php endif; ?>
