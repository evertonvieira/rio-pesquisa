<section class="view-company container">
  <div class="row">
    <div class="col-md-8">
      <header class="page-header">
        <figure class="image pull-right">
          <?php echo $this->Html->image("Company/{$company['Company']['id']}/{$company['Company']['id']}.{$company['Company']['ext']}", ['alt'=> $company['Company']['title'],  'class'=>'img-rounded img-thumbnail hidden-xs' ] ); ?>
        </figure>
        <h1><?= $company['Company']['title']; ?></h1>
      </header>
      <article class="jumbotron">
        <?= $company['Company']['description']; ?>
      </article>
      <?php if ($company['Image']):?>
        <section id="galery" class="content-thumbs">
          <h3 class="box-title">
            <span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
            Galeria
           </h3>
          <ul class="thumbs">
            <div class="row">
              <?php foreach ($company['Image'] as $key => $image): ?>
                <li class="col-md-3 col-xs-3">
                  <a href="<?php echo "/img/Company/{$company['Company']['id']}/Images/{$image['id']}.{$image['ext']}" ?>" class="colorbox">
                    <?php
                      echo $this->Thumbnail->render("Company/{$company['Company']['id']}/Images/{$image['id']}.{$image['ext']}",
                      array( 'path' => '', 'width' => '120', 'height' => '100', 'quality' => '100'
                      ),
                      array('id' => 'img-test', 'alt' => $company['Company']['title']));
                    ?>
                  </a>
                </li>
              <?php endforeach; ?>
            </div>
          </ul>
        </section>
      <?php endif; ?>
      <div class="comments">
        <h3 class="box-title">
          <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
          Comentários
         </h3>
        <div>
          <div class="fb-comments" data-href="<?php echo Router::url( $this->here, true ); ?>" width="100%" data-numposts="5"></div>
        </div>
      </div>

    </div>
    <div class="col-md-4" id="sidebar">
      <!-- About Me Box -->
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="box-title">
            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
            Sobre a empresa
          </h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body">
          <strong><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Localização</strong>
          <p class="text-muted"><?= $company['Company']['rua']; ?>, n° <?= $company['Company']['number']; ?>, <?= $company['District']['title']; ?>, <?= $company['District']['City']['title']; ?></p>
          <hr>
          <?php if ($company['Company']['telefone']): ?>
            <strong><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> Telefone</strong>
            <p class="text-muted"><?= $company['Company']['telefone']; ?></p>
            <hr>
          <?php endif;  ?>
          <?php if ($company['Company']['whatsapp']): ?>
            <strong><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> Whatsapp</strong>
            <p class="text-muted"><?= $company['Company']['whatsapp']; ?></p>
            <hr>
          <?php endif;  ?>
          <?php if ($company['Company']['email']): ?>
            <strong><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> E-mail</strong>
            <p class="text-muted"><?= $company['Company']['email']; ?></p>
          <?php endif;  ?>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      <section class="panel panel-default" id="mapa">
        <div class="panel-heading">
          <h3 class="box-title">
            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Mapa
          </h3>
        </div>
        <div class="panel-body">
          <?= $company['Company']['mapa']; ?>
        </div>
      </section>

      <div class="banner" style="display:block; overflow:hidden;">
        <!-- sidebar-interna -->
        <!-- Pesquisa Com.com -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-5954774589011046"
             data-ad-slot="9330498370"
             data-ad-format="auto"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </div>

    </div>
  </div>
</section>

<?php
  echo $this->Html->script('jquery.colorbox-min', array('inline'=>true));
  echo $this->Html->css('colorbox', array('inline'=>true));
?>

<script type="text/javascript">
  $(document).ready(function(){
    $(".colorbox").colorbox({
      rel:'group3',
      slideshow:true,
      innerWidth:840,
      transition:"fade"
    });
  });
</script>
