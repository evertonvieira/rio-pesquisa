
<?php
  echo $this->element('slide',
    [
      'slugSegment'=>$segment['Segment']['slug'],
      'plan_id' => 2,
      'position' => 0
    ]
  );
?>
<hr class="divided" />
<section class="container">
  <div class="row boxes">
    <div id="area-gold">
      <?php $companiesPremium = $this->requestAction("companies/getCompanies/{$this->params->slug}/2/all/6");
        foreach ($companiesPremium as $key => $premium):
      ?>
        <article class="box col-xs-6 col-md-4">
          <div class="box-overlay">
            <figure class="image">
              <?php echo $this->Html->link($this->Html->image("Company/{$premium['Company']['id']}/{$premium['Company']['id']}.{$premium['Company']['ext']}", ['class'=>'img-responsive', 'alt'=>$premium['Company']['title']] ), array('plugin'=>false, 'controller'=> 'companies', 'action'=>'view', 'segment'=> $premium['Segment']['slug'], 'slug'=>$premium['Company']['slug']), array('escape' => false));?>
            </figure>
          </div>
          <h2>
            <?php echo $this->Html->link($premium['Company']['title'], array('plugin'=>false, 'controller' => 'companies', 'action'=>'view', 'segment'=> $premium['Segment']['slug'], 'slug'=>$premium['Company']['slug'] ), array('escape' => false));?>
          </h2>
          <address>
            <p>
              <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?= $premium['Company']['rua']; ?>, n° <?= $premium['Company']['number']; ?>, <?= $premium['District']['title']; ?></h3>
            </p>
            <?php if ($premium['Company']['telefone']): ?>
              <p>
                <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> <?= $premium['Company']['telefone']; ?>
              </p>
            <?php endif; ?>
            <?php if ($premium['Company']['email']): ?>
              <p class="info_email">
                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> <?= $premium['Company']['email']; ?>
              </p>
            <?php endif; ?>
          </address>
        </article>
      <?php endforeach; ?>
      <article class="col-xs-6 col-md-4">
        <!-- bloco-gold -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-5954774589011046"
             data-ad-slot="9330498370"
             data-ad-format="auto"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </article>
    </div>
  </div>
</section>

<hr class="divided" />
<section class="container">
  <div class="row boxes" id="area-prata">
    <?php $companiesPremium = $this->requestAction("companies/getCompanies/{$this->params->slug}/3/all/8");
      foreach ($companiesPremium as $key => $premium):
    ?>
      <article class="box col-xs-6 col-md-3">
        <div class="box-overlay">
          <figure class="image">
            <?php echo $this->Html->link($this->Html->image("Company/{$premium['Company']['id']}/{$premium['Company']['id']}.{$premium['Company']['ext']}", ['class'=>'img-responsive', 'alt'=>$premium['Company']['title']] ), array('plugin'=>false, 'controller'=> 'companies', 'action'=>'view', 'segment'=> $premium['Segment']['slug'], 'slug'=>$premium['Company']['slug']), array('escape' => false));?>
          </figure>
        </div>
        <h2>
          <?php echo $this->Html->link($premium['Company']['title'], array('plugin'=>false, 'controller' => 'companies', 'action'=>'view', 'segment'=> $premium['Segment']['slug'], 'slug'=>$premium['Company']['slug'] ), array('escape' => false));?>
        </h2>
        <address>
          <p>
            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?= $premium['Company']['rua']; ?>, n° <?= $premium['Company']['number']; ?>, <?= $premium['District']['title']; ?></h3>
          </p>
          <?php if ($premium['Company']['telefone']): ?>
            <p>
              <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> <?= $premium['Company']['telefone']; ?>
            </p>
          <?php endif; ?>
        </address>
      </article>
    <?php endforeach; ?>
    <article class="col-xs-6 col-md-12">
      <!-- bloco-prata -->
      <!-- Pesquisa Com.com Horizontal -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:728px;height:90px"
             data-ad-client="ca-pub-5954774589011046"
             data-ad-slot="4467563245"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </article>
  </div>
</section>
<hr class="divided" />
<?php echo $this->element("box-plano-bronze"); ?>
