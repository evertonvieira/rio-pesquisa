let gulp = require('gulp');
let csso = require('gulp-csso');
let concat = require('gulp-concat');
let jsmin = require('gulp-jsmin');

const dirCss = 'webroot/css';
const dirJs = 'webroot/js';

gulp.task('css', () => {
  gulp.src(
    [
      dirCss + '/bootstrap.css',
      dirCss + '/jquery.bxslider.min.css',
      dirCss + '/owl.carousel.min.css',
      dirCss + '/owl.theme.default.min.css',
      dirCss + '/select2.min.css',
      dirCss + '/select2-bootstrap.min.css',
      dirCss + '/style.css',
    ]
  )
  .pipe(csso())
  .pipe(concat('assets.min.css'))
  .pipe(gulp.dest(dirCss))
  console.log("Concatenando e minificando css!");
});

gulp.task('js', () => {
  gulp.src(
    [
      dirJs + '/jquery-3.1.1.min.js',
      dirJs + '/bootstrap.js',
      dirJs + '/jquery.bxslider.min.js',
      dirJs + '/owl.carousel.min.js',
      dirJs + '/select2.full.js',
      dirJs + '/jquery.maskedinput.min.js',
      dirJs + '/scripts.js',
    ]
  )
  .pipe(jsmin())
  .pipe(concat('assets.min.js'))
  .pipe(gulp.dest(dirJs))
  console.log("Concatenando e minificando js!");
});

gulp.task('default', ['css', 'js']);
