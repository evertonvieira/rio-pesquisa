<?php
  $id = isset($id)?$id:false;
  $menu = $this->requestAction("/menus/load/{$id}");
  foreach ($menu['MenuSection'] as $section){
    if( count($section['MenuSectionLink'])>0 ){
      $menus['sessoes'][ $section['id'] ] = array( 'label'=> $section['name'], 'icon'=>$section['icon'] );
      foreach ( $section['MenuSectionLink'] as $link){
      $params = Router::parse( $link['alias']);
        $menus['sessoes'][ $section['id'] ]['links'][] = array(
          'alias'=> $link['alias'],
          'label'=>__( $link['name'] ),
          'icon'=> $link['icon'],
          'link'=> $params,
          'title'=> $link['name']
        );
      }
    }
  }
?>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo $this->Formatacao->get_gravatar(AuthComponent::user('email'));?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo AuthComponent::user('name');?></p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header">MENU</li>
      <li>
        <?php
          echo $this->Html->link('<i class="fa fa-dashboard"></i> <span>Dashboard</span>', array('controller'=>'admin'),
          array('title'=>'Dashboard', 'escape'=> false) );
        ?>
      <li>
      <?php foreach( $menus['sessoes'] as $k => $menu ):
  			$O = Set::classicExtract( $menu['links'] , '{n}.alias' );
  			$controller = $this->request->params['controller'];
  			$key = array_search( $controller , $O);
  			$in = '';
  			if( is_numeric($key)){
  				$in = 'in';
  			}
  			?>
        <li class="treeview">
          <a href="#">
            <i class="<?=$menu['icon'];?>"></i> <span><?=$menu['label'];?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php foreach( $menu['links'] as $link){ ?>
							<li><?php
								echo $this->Html->link("<i class=\"{$link['icon']}\"></i>{$link['label']}",
									$link['link'],
									array('title'=>$link['title'] , 'escape'=>false)
								);
							?></li>
						<?php } ?>
          </ul>
        </li>
      <?php endforeach;?>
      <li>
        <?php
          echo $this->Html->link('<i class="glyphicon glyphicon-home"></i> <span>Visualizar o site</span>', array('controller'=>'home', 'action'=>'/', 'admin'=>false),
          array('title'=>'glyphicon glyphicon-home', 'target'=>'_blanck', 'escape'=> false) );
        ?>
      <li>
    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>
