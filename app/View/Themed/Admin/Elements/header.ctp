<header class="main-header">
  <!-- Logo -->
  <?php
    echo $this->Html->link('<span class="logo-mini"><b>PE</b>C</span><span class="logo-lg"><b>PesquisaCom</b></span>',
    array('controller'=>'/', 'plugin'=>false),array('class'=>'logo','title'=>'Home', 'escape'=>false));
  ?>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <!--menu superior-->
    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
      <ul class="nav navbar-nav">
        <?php
          $menu = $this->requestAction("/menus/load/6");
          foreach ($menu['MenuSection'] as $k => $section):
            $cart = ($section['MenuSectionLink']) ? "<b class=\"caret\"></b>": "";
        ?>
          <li class="<?= ($section['MenuSectionLink'])? 'dropdown': '' ?>">
            <?php echo $this->Html->link("<i class=\"{$section['icon']}\"></i> {$section['name']} {$cart}" , $section['alias'], array('class'=>($section['MenuSectionLink']) ? "dropdown-toggle": "", 'data-toggle'=>'dropdown', 'escape' => false));?>
            <?php if ($section['MenuSectionLink']): ?>
              <ul class="dropdown-menu" role="menu">
                <?php foreach ($section['MenuSectionLink'] as $key => $link):?>
                  <li>
                    <?php echo $this->Html->link("<i class=\"{$link['icon']}\"></i> {$link['name']}" , $link['alias'], array('escape' => false));?>
                  </li>
                <?php endforeach; ?>
              </ul>
            <?php endif; ?>

          </li>
        <?php endforeach; ?>

      </ul>
    </div>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        <li class="dropdown messages-menu">

          <?php $total = $this->RequestAction("contacts/totalMensage");
          if ($total):
            ?>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
                <span class="label label-success">
                  <?= $total ?>
                </span>
            </a>
          <?php endif; ?>
          <ul class="dropdown-menu">
            <?php if ($total): ?>
              <li class="header">Você tem  <b><?= $total;?></b> novas mensagens</li>
            <?php endif; ?>
            <li>
              <ul class="menu">
                <?php $data = $this->RequestAction("contacts/notifications");?>
      					<?php foreach ($data as $msg):?>
                  <li>
                    <a href="<?php echo $this->Html->url('/', true)?>admin/contacts/view/<?=$msg['Contact']['id'];?>">
                      <h4>
                        <?=$msg['Contact']['name'];?>
                        <small><i class="fa fa-clock-o"></i> <?=$this->Formatacao->tempo($msg['Contact']['created']);?></small>
                      </h4>
                      <p><?=$this->Formatacao->LimitaCaracter($msg['Contact']['message'], 80);?></p>
                    </a>
                  </li>
                <?php endforeach;?>
              </ul>

            </li>
            <li class="footer">
  						<?php echo $this->Html->link('Todas as mensagens', array('controller'=>'contacts', 'action'=>'index', 'admin'=>true, 'plugin'=>false),array('title'=>'Leia todas as novas mensagens', 'escape'=>false));?>
  					</li>
          </ul>
        </li>
        <!-- /.messages-menu -->

        <!-- Notifications Menu -->
        <li class="dropdown notifications-menu">
          <!-- Menu toggle button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            <span class="label label-warning">10</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">You have 10 notifications</li>
            <li>
              <!-- Inner Menu: contains the notifications -->
              <ul class="menu">
                <li><!-- start notification -->
                  <a href="#">
                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                  </a>
                </li>
                <!-- end notification -->
              </ul>
            </li>
            <li class="footer"><a href="#">View all</a></li>
          </ul>
        </li>
        <!-- Tasks Menu -->
        <li class="dropdown tasks-menu">
          <!-- Menu Toggle Button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-flag-o"></i>
            <span class="label label-danger">9</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">You have 9 tasks</li>
            <li>
              <!-- Inner menu: contains the tasks -->
              <ul class="menu">
                <li><!-- Task item -->
                  <a href="#">
                    <!-- Task title and progress text -->
                    <h3>
                      Design some buttons
                      <small class="pull-right">20%</small>
                    </h3>
                    <!-- The progress bar -->
                    <div class="progress xs">
                      <!-- Change the css width attribute to simulate progress -->
                      <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                        <span class="sr-only">20% Complete</span>
                      </div>
                    </div>
                  </a>
                </li>
                <!-- end task item -->
              </ul>
            </li>
            <li class="footer">
              <a href="#">View all tasks</a>
            </li>
          </ul>
        </li>
        <!-- User Account Menu -->
        <li class="dropdown">
  				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo AuthComponent::user('name');?> <b class="caret"></b></a>
  				<ul class="dropdown-menu">
  					<li class="divider"></li>
  					<li>
  						<?php
  							echo $this->Html->link('<i class="fa fa-fw fa-cog"></i> Configurações',	array('controller'=> 'settings',	'action'=>'index', 'admin'=>true),
  							array('escape' => false, 'title' => 'Configurações'));
  						?>
  					</li>
  					<li>
  						<?php
  							echo $this->Html->link('<i class="fa fa-fw fa-power-off"></i> Log Out',	array('controller'=> 'users',	'action'=>'logout',	'plugin'=>false, 'admin'=>false),
  							array('escape' => false, 'title' => 'Log Out'));
  						?>
  					</li>
  				</ul>
  			</li>
      </ul>
    </div>
  </nav>
</header>
