<?php echo $this->Form->create('User', array('class'=>'form-horizontal col-md-6')); ?>
	<div class="box-body">
		<?php echo $this->Form->input('id');?>

	  <div class="form-group">
	    <label class="col-sm-2 control-label">Nome:</label>
	    <div class="col-sm-10">
				<div class=" has-feedback">
					<?php echo $this->Form->input('name', array('placeholder'=>'nome', 'class'=>'form-control', 'label'=>false));	?>
	        <span class="fa fa-user form-control-feedback"></span>
	      </div>
	    </div>
	  </div>
		<div class="form-group">
	    <label class="col-sm-2 control-label">E-mail:</label>
	    <div class="col-sm-10">
				<div class=" has-feedback">
					<?php echo $this->Form->input('email', array('placeholder'=>'e-mail', 'class'=>'form-control', 'label'=>false));	?>
	        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	      </div>
	    </div>
	  </div>
		<?php	if ($this->request['action'] == "admin_add"): ?>
			 <div class="form-group">
				 <label class="col-sm-2 control-label">Password:</label>
				 <div class="col-sm-10">
					 <div class=" has-feedback">
						 <?php echo $this->Form->input('password', array('placeholder'=>'password', 'class'=>'form-control', 'label'=>false));	?>
						 <span class="glyphicon glyphicon-lock form-control-feedback"></span>
					 </div>
				 </div>
			 </div>
		<?php endif;?>
		<div class="form-group">
	    <label class="col-sm-2 control-label">Status:</label>
	    <div class="col-sm-10">
				<div class=" has-feedback">
				  <?php
				    echo $this->Form->input("status", array(
				      'type' => 'select',
				      'label'=>false,
				      'options' => array(0=>'ativo', 1=>'inativo'),
				      'escape'=>false,
				      'class'=>'form-control'
				    ));
				  ?>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="box-footer">
		<?php echo $this->Form->button('Salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-primary', 'escape' => false)); ?>
	</div>
<?php echo $this->Form->end(); ?>
