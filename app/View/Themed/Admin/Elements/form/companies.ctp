<?php echo $this->element("tinymce"); ?>
<?php echo $this->Form->create('Company', array('type' => 'file')); ?>
  <div class="box-body row">
    <div class="col-md-9">
      <div class="form-group">
        <?php echo $this->Form->input('id');?>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('title', array('label'=>'Nome da Companhia/Comércio:', 'class'=>'form-control input-lg'));?>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('cnpj', array('label'=>'CNPJ(opcional):', 'class'=>'form-control Cnpj', 'placeholder'=>'00.000.000/0000-00'));?>
      </div>

      <div class="form-group">
        <?php echo $this->Form->input('customer_id', array('label'=>'Cliente/Responsável:', 'class'=>'SelectTag form-control'));?>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('description', array('label'=>'Descrição:', 'type'=>'textarea', 'class'=>'editor form-control', 'rows'=>12));?>
      </div>
      <div class="form-group">
        <?php
          echo $this->Form->input('imagem', array( 'type'=>'file', 'class'=>'form-control file', 'label' =>'Imagem:'));
        ?>
      </div>
      <div class="form-group">
        <?php
          echo $this->Form->input('mobile', array( 'type'=>'file', 'class'=>'form-control file', 'label' =>'Logo Mobile:'));
        ?>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('segment_id', array('label'=>'Segmento da Companhia:', 'class'=>'SelectTag form-control'));?>
      </div>
      <div class="form-group">
        <label>Status:</label>
        <?php
        echo $this->Form->input("active", array(
          'type' => 'select',
          'label'=>false,
          'options' => array(0=>'Ativado', 1=>'Desativado'),
          'escape'=>false,
          'class'=>'form-control'
        ));
        ?>
      </div>

      <div class="form-group">
        <?php echo $this->Form->input('plan_id', array('label'=>'Plano:', 'class'=>'form-control'));?>
      </div>
      <?php
        echo $this->Seo->form();
      ?>
    </div>

    <div class="col-md-3">

      <div class="page-header">
        <h4>Localização:</h4>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('rua', array('label'=>'Nome da Rua/Avenida:', 'class'=>'form-control', 'placeholder'=>'rua/avenida'));?>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('district_id', array('label'=>'Bairro:', 'class'=>'form-control SelectTag', 'placeholder'=>'district_id'));?>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('number', array('label'=>'Número do estabelecimento:', 'class'=>'form-control', 'placeholder'=>'número'));?>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('mapa', array('label'=>'Iframe do mapa do Google:', 'class'=>'form-control input-lg'));?>
      </div>

      <div class="page-header">
        <h4>Dados de contato:</h4>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('telefone', array('label'=>'Telefone Fixo(Opcional):', 'class'=>'TelefoneFixo form-control'));?>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('whatsapp', array('label'=>'Whatsapp:', 'class'=>'Celular form-control'));?>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('email', array('label'=>'E-mail:', 'class'=>'form-control'));?>
      </div>

    </div>


  </div>


  <div class="box-footer">
    <?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-success btn-lg', 'escape' => false)); ?>
  </div>
<?php echo $this->Form->end(); ?>
