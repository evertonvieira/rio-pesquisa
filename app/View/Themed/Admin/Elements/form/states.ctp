<?php echo $this->Form->create('State', array('role' => 'form')); ?>
  <div class="box-body">
    <div class="form-group">
      <?php echo $this->Form->input('id');?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('uf', array('label'=>'Unidade Federativa (UF):', 'class'=>'form-control', 'placeholder'=>'RJ'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('title', array('label'=>'Estado:', 'class'=>'form-control', 'placeholder'=>'Rio de Janeiro'));?>
    </div>
  </div>

  <div class="box-footer">
    <?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-success btn-lg', 'escape' => false)); ?>
  </div>
<?php echo $this->Form->end(); ?>
