<?php echo $this->element("tinymce"); ?>
<?php echo $this->Form->create('Segment', array('role' => 'form')); ?>
  <div class="box-body">
    <div class="row">
      <div class="col-md-8">
        <div class="form-group">
          <?php echo $this->Form->input('id');?>
        </div>
        <div class="form-group">
          <?php echo $this->Form->input('title', array('label'=>'Título do segmento:', 'class'=>'form-control input-lg'));?>
        </div>

        <div class="form-group">
          <?php echo $this->Form->input('description', array('label'=>'Descrição (opcional):', 'type'=>'textarea', 'class'=>'editor form-control', 'rows'=> 8));?>
        </div>
        <div class="form-group">
          <label>Status:</label>
          <?php
            echo $this->Form->input("active", array(
              'type' => 'select',
              'label'=>false,
              'options' => array(0=>'Ativado', 1=>'Desativado'),
              'escape'=>false,
              'class'=>'form-control'
            ));
          ?>
        </div>
      </div>
      <div class="col-md-4">
        <?php
          echo $this->Seo->form();
        ?>
      </div>
    </div>
  </div>

  <div class="box-footer">
    <?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-success btn-lg', 'escape' => false)); ?>
  </div>

<?php echo $this->Form->end(); ?>
