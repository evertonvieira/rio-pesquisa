<?php echo $this->Form->create('City', array('role' => 'form')); ?>
  <div class="box-body">
    <div class="form-group">
      <?php echo $this->Form->input('id');?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('title', array('label'=>'Nome da cidade:', 'class'=>'form-control', 'placeholder'=>'Rio de Janeiro'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('state_id', array('label'=>'Estado:', 'class'=>'SelectTag form-control', 'placeholder'=>'estado'));?>
    </div>
  </div>

  <div class="box-footer">
    <?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-success btn-lg', 'escape' => false)); ?>
  </div>
<?php echo $this->Form->end(); ?>
