<?php echo $this->Form->create('Menu', array('role'=>'form')); ?>
	<div class="box-body">
		<?php echo $this->Form->input('id');?>
		<div class="form-group">
			<?php echo $this->Form->input('name', array('type'=>'text','label'=>'Título do Menu:', 'class'=>'input-lg form-control'));?>
		</div>
	</div>
	<div class="box-footer">
		<?php echo $this->Form->button('Salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-primary', 'escape' => false)); ?>
	</div>
<?php echo $this->Form->end(); ?>
