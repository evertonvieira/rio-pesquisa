<?php echo $this->Form->create('District', array('role' => 'form')); ?>
  <div class="box-body">
    <div class="form-group">
      <?php echo $this->Form->input('id');?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('title', array('label'=>'Nome do Bairro:', 'class'=>'form-control', 'placeholder'=>'nome bairro'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('city_id', array('label'=>'Cidade:', 'class'=>'SelectTag form-control'));?>
    </div>
    <div class="form-group">
      <label>Status:</label>
      <?php
        echo $this->Form->input("active", array(
          'type' => 'select',
          'label'=>false,
          'options' => array(0=>'Ativado', 1=>'Desativado'),
          'escape'=>false,
          'class'=>'form-control'
        ));
      ?>
    </div>
  </div>

  <div class="box-footer">
    <?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-success btn-lg', 'escape' => false)); ?>
  </div>
<?php echo $this->Form->end(); ?>
