<?php echo $this->element("tinymce"); ?>
<?php echo $this->Form->create('Customer', array('role' => 'form')); ?>
<div class="box-body row">
  <div class="col-md-8">
    <div class="form-group">
      <?php echo $this->Form->input('id');?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('titular', array('label'=>'Nome do titular:', 'class'=>'form-control input-lg', 'placeholder'=>'nome do titular'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('cpf', array('label'=>'CPF:', 'class'=>'Cpf form-control'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('data_nascimento', array('label'=>'Data de nascimento:', 'type'=>'text','class'=>'DataNascimento form-control', 'placeholder'=>'00/00/0000'));?>
    </div>
    <div class="form-group">
      <label>Gênero:</label>
      <?php
      echo $this->Form->input("genero", array(
        'type' => 'select',
        'label'=>false,
        'options' => array(0=>'Masculino', 1=>'Feminino'),
        'escape'=>false,
        'class'=>'form-control'
      ));
      ?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('obs', array('label'=>'Observação sobre o cliente:', 'type'=>'textarea','class'=>'editor form-control', 'rows'=>8));?>
    </div>
  </div>
  <div class="col-md-4">
    <header class="page-header">
      <h3>Dados de contato:</h3>
    </header>
    <div class="form-group">
      <?php echo $this->Form->input('email', array('label'=>'Email:', 'class'=>'form-control', 'placeholder'=>'example@example.com'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('phone_fixo', array('label'=>'Telefone fixo:', 'class'=>'TelefoneFixo form-control'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('celular', array('label'=>'Celular:', 'class'=>'Celular form-control'));?>
    </div>
  </div>
</div>

<div class="box-footer">
  <?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-lg btn-success', 'escape' => false)); ?>
</div>
<?php echo $this->Form->end(); ?>
