<?php echo $this->Form->create('MenuSection', array('role'=>'form')); ?>
	<div class="box-body">
		<div class="form-group">
			<?php echo $this->Form->input('id');?>
		</div>
		<div class="form-group">
			<?php echo $this->Form->input('name', array('type'=>'text','label'=>'Título do Menu:', 'class'=>'input-lg form-control'));?>
		</div>
		<div class="form-group">
			<label><?php echo __d('O', 'Alias:'); ?></label>
			<?php
				echo $this->Form->input('alias', array('class'=>'form-control', 'label'=>false));
			?>
		</div>
		<div class="form-group">
			<label><?php echo __d('O', 'Menu pai:'); ?></label>
			<?php
				echo $this->Form->input('menu_id', array('class'=>'form-control', 'label'=>false));
			?>
		</div>
		<div class="bs-glyphicons">
			<?php echo $this->element("form/icon_options");?>
		</div>
	</div>
	<div class="box-footer">
		<?php echo $this->Form->button('Salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-primary', 'escape' => false)); ?>
	</div>
<?php echo $this->Form->end(); ?>
