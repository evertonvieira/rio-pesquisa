<div class="btn-group" style="width: 100%; margin-bottom: 10px;">
  <label>Selecione um ícone:</label>
  <ul class="fc-color-picker" id="Icons">
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-edit">
        <span class="text-blue square-icon"  id="rrewerg">
          <i class="fa fa-edit"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-envelope">
        <span class="text-blue square-icon">
          <i class="fa fa-envelope"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-users">
        <span class="text-blue square-icon" >
          <i class="fa fa-users"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-inbox">
        <span class="text-blue square-icon" >
          <i class="fa fa-inbox"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="ion-ios-stopwatch">
        <span class="text-blue square-icon" >
          <i class="ion-ios-stopwatch"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-commenting">
        <span class="text-blue square-icon" >
          <i class="fa fa-commenting"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-map-o">
        <span class="text-blue square-icon" >
          <i class="fa fa-map-o"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-camera">
        <span class="text-blue square-icon" >
          <i class="fa fa-camera"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-database">
        <span class="text-blue square-icon" >
          <i class="fa fa-database"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-download">
        <span class="text-blue square-icon" >
          <i class="fa fa-download"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-external-link">
        <span class="text-blue square-icon" >
          <i class="fa fa-external-link"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-user-plus">
        <span class="text-blue square-icon" >
          <i class="fa fa-user-plus"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-image">
        <span class="text-blue square-icon" >
          <i class="fa fa-image"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-calendar">
        <span class="text-blue square-icon" >
          <i class="fa fa-calendar"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-home">
        <span class="text-blue square-icon" >
          <i class="fa fa-home"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-paint-brush">
        <span class="text-blue square-icon" >
          <i class="fa fa-paint-brush"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-server">
        <span class="text-blue square-icon" >
          <i class="fa fa-server"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-tasks">
        <span class="text-blue square-icon" >
          <i class="fa fa-tasks"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-trash">
        <span class="text-blue square-icon" >
          <i class="fa fa-trash"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-wrench">
        <span class="text-blue square-icon" >
          <i class="fa fa-wrench"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-list">
        <span class="text-blue square-icon" >
          <i class="fa fa-list"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-bars">
        <span class="text-blue square-icon" >
          <i class="fa fa-bars"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-cogs">
        <span class="text-blue square-icon" >
          <i class="fa fa-cogs"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-globe">
        <span class="text-blue square-icon" >
          <i class="fa fa-globe"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-credit-card">
        <span class="text-blue square-icon" >
          <i class="fa fa-credit-card"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-money">
        <span class="text-blue square-icon" >
          <i class="fa fa-money"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-mobile">
        <span class="text-blue square-icon" >
          <i class="fa fa-mobile"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-file-text">
        <span class="text-blue square-icon" >
          <i class="fa fa-file-text"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-line-chart">
        <span class="text-blue square-icon" >
          <i class="fa fa-line-chart"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-sitemap">
        <span class="text-blue square-icon" >
          <i class="fa fa-sitemap"></i>
        </span>
      </label>
    </li>

    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-plus">
        <span class="text-blue square-icon" >
          <i class="fa fa-plus"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-list-alt">
        <span class="text-blue square-icon" >
          <i class="fa fa-list-alt"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-user">
        <span class="text-blue square-icon" >
          <i class="fa fa-user"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="glyphicon glyphicon-phone">
        <span class="text-blue square-icon" >
          <i class="glyphicon glyphicon-phone"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="fa fa-group">
        <span class="text-blue square-icon" >
          <i class="fa fa-group"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="ion-person">
        <span class="text-blue square-icon" >
          <i class="ion-person"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="ion-person-stalker">
        <span class="text-blue square-icon" >
          <i class="ion-person-stalker"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="ion-ios-people">
        <span class="text-blue square-icon" >
          <i class="ion-ios-people"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="ion-chatbubbles">
        <span class="text-blue square-icon" >
          <i class="ion-chatbubbles"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="ion-briefcase">
        <span class="text-blue square-icon" >
          <i class="ion-briefcase"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="glyphicon glyphicon-cutlery">
        <span class="text-blue square-icon">
          <i class="glyphicon glyphicon-cutlery"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="ion-android-desktop">
        <span class="text-blue square-icon" >
          <i class="ion-android-desktop"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="ion-ios-toggle">
        <span class="text-blue square-icon" >
          <i class="ion-ios-toggle"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="glyphicon glyphicon-map-marker">
        <span class="text-blue square-icon" >
          <i class="glyphicon glyphicon-map-marker"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="glyphicon glyphicon-list-alt">
        <span class="text-blue square-icon" >
          <i class="glyphicon glyphicon-list-alt"></i>
        </span>
      </label>
    </li>
    <li>
      <label class="radio-inline">
        <input type="radio" name="data[<?php echo Inflector::singularize($this->name);?>][icon]" class="radioIcon" value="glyphicon glyphicon-map-marker">
        <span class="text-blue square-icon" >
          <i class="glyphicon glyphicon-map-marker"></i>
        </span>
      </label>
    </li>
  </ul>
</div>
