<?php echo $this->element("tinymce"); ?>
<?php echo $this->Form->create('Plan', array('role' => 'form')); ?>
  <div class="box-body">
    <div class="form-group">
      <?php echo $this->Form->input('id', array('label'=>'Id:', 'class'=>'form-control', 'placeholder'=>'id'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('name', array('label'=>'Name:', 'class'=>'form-control', 'placeholder'=>'name'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('price', array('label'=>'Price:', 'class'=>'form-control', 'placeholder'=>'price'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('description', array('label'=>'Descrição:', 'rows'=>8, 'type'=>'textarea','class'=>'editor form-control', 'placeholder'=>'description'));?>
    </div>
  </div>
  <div class="box-footer">
    <?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-lg btn-success', 'escape' => false)); ?>
  </div>
<?php echo $this->Form->end(); ?>
