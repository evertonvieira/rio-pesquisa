<?php echo $this->element("tinymce"); ?>
<script type="text/javascript">
  $(document).ready( () => {
    $("#PaymentStatus").on('change', ()=>{
      $('#PaymentStatus').each(function() {
        if($(this).val() == 1) {
          $('.data_pago').show()
        }else{
          $('.data_pago').hide()
        }
      });
    })
  });
</script>
<?php echo $this->Form->create('Payment', array('role' => 'form')); ?>
  <div class="box-body">
  <?php echo $this->Form->input('id');?>
  <div class="form-group">
    <?php echo $this->Form->input('type', array('label'=>'Tipo:', 'type'=>'select', 'class'=>'form-control input-lg', 'options'=>[0=> 'Entrada', 1=> 'Saída']));?>
  </div>
  <div class="form-group">
    <?php echo $this->Form->input('status', array('label'=>'Status do pagamento:', 'type'=>'select', 'class'=>'form-control input-lg', 'options'=>[0=> 'Aguardando Pagamento', 1=> 'Fatura Paga', 2=>'Fatura Vencida']));?>
  </div>
  <div class="form-group">
    <?php echo $this->Form->input('vencimento', array('label'=>'Data de Vencimento:', 'class'=>'form-control DataNascimento', 'placeholder'=>'00/00/0000', 'type'=>'text'));?>
  </div>
  <div class="form-group data_pago" style="display:none;">
    <?php echo $this->Form->input('data_pago', array('label'=>'Data do pagamento:', 'class'=>'form-control DataNascimento', 'placeholder'=>'00/00/0000', 'type'=>'text'));?>
  </div>
  <div class="form-group">
    <?php echo $this->Form->input('value', array('label'=>'Valor da Fatura:', 'class'=>'form-control Moeda input-lg', 'placeholder'=>'R$', 'type'=>'text'));?>
  </div>
  <div class="form-group">
    <?php echo $this->Form->input('method', array('label'=>'Metodo de pagamento:', 'class'=>'form-control', 'type'=>'select', 'options'=>[0=>'Pagamento em Dinheiro', 1=>'Cartão de Crédito', 2=>'Cartão de Débido']));?>
  </div>
  <div class="form-group">
    <?php echo $this->Form->input('obs', array('label'=>'Observação (opcional):', 'class'=>'form-control editor', 'type'=>'textarea'));?>
  </div>
  </div>

  <div class="box-footer">
    <?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> Salvar', array('type' => 'submit', 'class'=> 'btn btn-large btn-success btn-lg', 'escape' => false)); ?>
  </div>
<?php echo $this->Form->end(); ?>
