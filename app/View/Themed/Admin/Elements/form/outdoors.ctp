<?php echo $this->Form->create('Outdoor', array('type' => 'file')); ?>
  <div class="box-body">
    <div class="form-group">
      <?php echo $this->Form->input('title', array('label'=>'Título do Outdoor:', 'class'=>'input-lg form-control'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('link', array('label'=>'Link do Outdoor:', 'class'=>'form-control', 'placeholder'=>'http://'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('date', array('label'=>'Data para publicar:', 'class'=>'form-control DataNascimento', 'placeholder'=>'00/00/0000', 'type'=>'text'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('imagem', array('label'=>'Imagem:', 'class'=>'form-control', 'type'=>'file'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('company_id', array('label'=>'Companhia/Comércio:', 'class'=>'SelectTag form-control', 'placeholder'=>'company_id'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('plan_id', array('label'=>'Plano:', 'class'=>'form-control', 'placeholder'=>'plan_id'));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('segment_id', array('label'=>'Segmento:', 'class'=>'form-control SelectTag'));?>
    </div>
    <div class="form-group">
      <label>Status:</label>
      <?php
        echo $this->Form->input("active", array(
          'type' => 'select',
          'label'=>false,
          'options' => array(0=>'Ativado', 1=>'Desativado'),
          'escape'=>false,
          'class'=>'form-control'
        ));
      ?>
    </div>
    <div class="form-group">
      <label>Status:</label>
      <?php
        echo $this->Form->input("position", array(
          'type' => 'select',
          'label'=>false,
          'options' => array(0=>'slide site', 1=>'slide mobile'),
          'escape'=>false,
          'class'=>'form-control'
        ));
      ?>
    </div>
  </div>

  <div class="box-footer">
    <?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-success btn-lg', 'escape' => false)); ?>
  </div>
<?php echo $this->Form->end(); ?>
