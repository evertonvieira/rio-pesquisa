<script>
$(document).ready(function(){
	$('.nav-tabs a').click(function (e) {
		url = $(this).attr("rel");
		tab = $(this).attr("href");
		window.location = url;
		e.preventdefault();
	})
});
</script>

<div class="row">
	<div class="col-xs-12">
    <div class="box box-primary">
			<div class="box-body">
				<div>
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="<?php if($this->request->params['action'] == 'admin_index'){ echo "active";}?>"><a href="#home" rel="<?php echo $this->Html->url('/admin/settings/', true);?>" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
						<li role="presentation" class="<?php if($this->request->params['action'] == 'admin_seo'){ echo "active";}?>"><a href="#seo" rel="<?php echo $this->Html->url('/admin/settings/seo', true);?>" aria-controls="seo" role="tab" data-toggle="tab">Seo</a></li>
						<li role="presentation" class="<?php if($this->request->params['action'] == 'admin_profile'){ echo "active";}?>"><a href="#profile" rel="<?php echo $this->Html->url('/admin/settings/profile', true);?>" aria-controls="profile" role="tab" data-toggle="tab">Configurações de Usuário</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane <?php if($this->request->params['action'] == 'admin_index'){ echo "active";}?>" id="home">

						</div>
						<div role="tabpanel" class="tab-pane <?php if($this->request->params['action'] == 'admin_seo'){ echo "active";}?>" id="seo">
							<?php
								echo $this->Form->create('MetaTags', array('role' => 'form'));
							?>
								<?php
									echo $this->Seo->form();
								?>
								<div class="pull-left">
									<?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> Salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-success btn-lg', 'escape' => false)); ?>
								</div>
							<?php echo $this->Form->end(); ?>
						</div>
						<div role="tabpanel" class="tab-pane <?php if($this->request->params['action'] == 'admin_profile'){ echo "active";}?>" id="profile">
							<div class="page-header">
								<h3>Configurações:</h3>
							</div>

							<?php echo $this->Form->create('User', array('role'=>'form')); ?>
								<div class="form-group">
									<?php
										echo $this->Form->input('email',
											array(
												'label'=> 'E-mail:',
												'class'=>'form-control',
											)
										);
									?>
								</div>
								<div class="form-group">
									<?php
										echo $this->Form->input('name',
											array(
												'label'=>'Nome:',
												'class'=>'form-control',
											)
										);
									?>
									<br />
                </div>
                <div class="pull-left">
 									<?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> Salvar', array('type' => 'submit', 'class' => 'btn btn-large btn-success btn-lg', 'escape' => false)); ?>
 								</div>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
