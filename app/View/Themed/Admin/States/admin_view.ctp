
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><?= $title; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="box-body">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> editar'), array('action' => 'edit', $state['State']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
          <td><strong><?php echo __('id'); ?></strong></td>
      		<td>
      			<?php echo h($state['State']['id']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('unidade federativa'); ?></strong></td>
      		<td>
      			<?php echo h($state['State']['uf']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('estado'); ?></strong></td>
      		<td>
      			<?php echo h($state['State']['title']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('cadastrado'); ?></strong></td>
      		<td>
      			<?php echo $this->Formatacao->DataHora($state['State']['created']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('atualizado'); ?></strong></td>
      		<td>
      			<?php echo $this->Formatacao->DataHora($state['State']['updated']); ?>
      		</td>
        </tr>
      </tbody>
		</table>
	</div>
</div>
