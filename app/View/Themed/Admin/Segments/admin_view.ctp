
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><?php echo $title; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>

	<div class="box-body">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> editar'), array('action' => 'edit', $segment['Segment']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['title']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Slug'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['slug']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Description'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['description']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Active'); ?></strong></td>
		<td>
			<?php echo $this->Formatacao->active($segment['Segment']['active']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Updated'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['updated']); ?>
			&nbsp;
		</td>
</tr>			</tbody>
		</table>
	</div>

</div>
