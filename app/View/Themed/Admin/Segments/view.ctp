
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><= $title; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="box-body">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Edit'), array('action' => 'edit', $segment['Segment']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Slug'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['slug']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Description'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['description']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Active'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['active']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Updated'); ?></strong></td>
		<td>
			<?php echo h($segment['Segment']['updated']); ?>
			&nbsp;
		</td>
</tr>			</tbody>
		</table>
	</div>

	
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo __('Related Companies'); ?></h3>
				<div class="box-tools pull-right">
					<?php echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> '.__('New Company'), array('controller' => 'companies', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>				</div><!-- /.actions -->
			</div>
			<?php if (!empty($segment['Company'])): ?>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
										<th class="text-center"><?php echo __('Id'); ?></th>
		<th class="text-center"><?php echo __('Name'); ?></th>
		<th class="text-center"><?php echo __('Slug'); ?></th>
		<th class="text-center"><?php echo __('Description'); ?></th>
		<th class="text-center"><?php echo __('Active'); ?></th>
		<th class="text-center"><?php echo __('Customer Id'); ?></th>
		<th class="text-center"><?php echo __('Segment Id'); ?></th>
		<th class="text-center"><?php echo __('Plan Id'); ?></th>
		<th class="text-center"><?php echo __('Adress Id'); ?></th>
		<th class="text-center"><?php echo __('Created'); ?></th>
		<th class="text-center"><?php echo __('Updated'); ?></th>
								<th class="text-center"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
								<?php
								$i = 0;
								foreach ($segment['Company'] as $company): ?>
		<tr>
			<td class="text-center"><?php echo $company['id']; ?></td>
			<td class="text-center"><?php echo $company['name']; ?></td>
			<td class="text-center"><?php echo $company['slug']; ?></td>
			<td class="text-center"><?php echo $company['description']; ?></td>
			<td class="text-center"><?php echo $company['active']; ?></td>
			<td class="text-center"><?php echo $company['customer_id']; ?></td>
			<td class="text-center"><?php echo $company['segment_id']; ?></td>
			<td class="text-center"><?php echo $company['plan_id']; ?></td>
			<td class="text-center"><?php echo $company['adress_id']; ?></td>
			<td class="text-center"><?php echo $company['created']; ?></td>
			<td class="text-center"><?php echo $company['updated']; ?></td>
			<td class="text-center">
				<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('controller' => 'companies', 'action' => 'view', $company['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
				<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('controller' => 'companies', 'action' => 'edit', $company['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
				<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('controller' => 'companies', 'action' => 'delete', $company['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $company['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			<?php endif; ?>

		</div>
	
</div>
