<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title"></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> New Segment'), array('action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
		<table id="Table" class="table table-bordered table-striped table-responsive">
			<thead>
				<tr>
									<th><?php echo __("id"); ?></th>
									<th><?php echo __("name"); ?></th>
									<th><?php echo __("slug"); ?></th>
									<th><?php echo __("description"); ?></th>
									<th><?php echo __("active"); ?></th>
									<th><?php echo __("created"); ?></th>
									<th><?php echo __("updated"); ?></th>
									<th width="160"><?php echo __('ações'); ?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($segments as $segment): ?>
	<tr>
		<td class="text-left"><?php echo h($segment['Segment']['id']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($segment['Segment']['name']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($segment['Segment']['slug']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($segment['Segment']['description']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($segment['Segment']['active']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($segment['Segment']['created']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($segment['Segment']['updated']); ?>&nbsp;</td>
		<td>
		<div class="btn-group">
			<?php echo $this->Html->link(__('<i class="fa fa-eye"></i>'), array('action' => 'view', $segment['Segment']['id']), array('class' => 'btn btn-default', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
			<?php echo $this->Html->link(__('<i class="fa fa-edit"></i>'), array('action' => 'edit', $segment['Segment']['id']), array('class' => 'btn btn-primary', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
			<?php echo $this->Form->postLink(__('<i class="fa fa-trash"></i>'), array('action' => 'delete', $segment['Segment']['id']), array('class' => 'btn btn-danger', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $segment['Segment']['id'])); ?>
		</div>
		</td>
	</tr>
<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$("#Table").dataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Portuguese-Brasil.json"
			},
			"pageLength": 20
		});
	});
</script>
