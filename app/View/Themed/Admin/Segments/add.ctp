<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<?php echo $this->Form->create('Segment', array('role' => 'form')); ?>
  <div class="box-body">
							<div class="form-group">
						<?php echo $this->Form->input('name', array('label'=>'Name:', 'class'=>'form-control', 'placeholder'=>'name'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('slug', array('label'=>'Slug:', 'class'=>'form-control', 'placeholder'=>'slug'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('description', array('label'=>'Description:', 'class'=>'form-control', 'placeholder'=>'description'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('active', array('label'=>'Active:', 'class'=>'form-control', 'placeholder'=>'active'));?>
					</div>
  </div>
  
					<div class="box-footer">
						<?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> Save data', array('type' => 'submit', 'class' => 'btn btn-large btn-primary', 'escape' => false)); ?>
					</div>
  <?php echo $this->Form->end(); ?>
</div>
