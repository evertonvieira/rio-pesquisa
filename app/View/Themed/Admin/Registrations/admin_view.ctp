
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><= $title; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="box-body">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Edit'), array('action' => 'edit', $registration['Registration']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Titular'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['titular']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Data Nascimento'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['data_nascimento']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Genero'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['genero']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Email'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['email']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Telefone'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['telefone']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Nome Comercio'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['nome_comercio']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Endereco'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['endereco']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Telefone Contato'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['telefone_contato']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Whatsapp'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['whatsapp']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Email Contato'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['email_contato']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Titulo Comercio'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['titulo_comercio']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Site'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['site']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Segmento'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['segmento']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Ext'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['ext']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Description'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['description']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Updated'); ?></strong></td>
		<td>
			<?php echo h($registration['Registration']['updated']); ?>
			&nbsp;
		</td>
</tr>			</tbody>
		</table>
	</div>

	
</div>
