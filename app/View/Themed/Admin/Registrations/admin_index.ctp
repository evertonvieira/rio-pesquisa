<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title"></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> New Registration'), array('action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
		<table id="Table" class="table table-bordered table-striped table-responsive">
			<thead>
				<tr>
									<th><?php echo __("id"); ?></th>
									<th><?php echo __("titular"); ?></th>
									<th><?php echo __("data_nascimento"); ?></th>
									<th><?php echo __("genero"); ?></th>
									<th><?php echo __("email"); ?></th>
									<th><?php echo __("telefone"); ?></th>
									<th><?php echo __("nome_comercio"); ?></th>
									<th><?php echo __("endereco"); ?></th>
									<th><?php echo __("telefone_contato"); ?></th>
									<th><?php echo __("whatsapp"); ?></th>
									<th><?php echo __("email_contato"); ?></th>
									<th><?php echo __("titulo_comercio"); ?></th>
									<th><?php echo __("site"); ?></th>
									<th><?php echo __("segmento"); ?></th>
									<th><?php echo __("ext"); ?></th>
									<th><?php echo __("description"); ?></th>
									<th><?php echo __("created"); ?></th>
									<th><?php echo __("updated"); ?></th>
									<th width="160"><?php echo __('ações'); ?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($registrations as $registration): ?>
	<tr>
		<td class="text-left"><?php echo h($registration['Registration']['id']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['titular']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['data_nascimento']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['genero']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['email']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['telefone']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['nome_comercio']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['endereco']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['telefone_contato']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['whatsapp']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['email_contato']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['titulo_comercio']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['site']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['segmento']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['ext']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['description']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['created']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($registration['Registration']['updated']); ?>&nbsp;</td>
		<td>
		<div class="btn-group">
			<?php echo $this->Html->link(__('<i class="fa fa-eye"></i>'), array('action' => 'view', $registration['Registration']['id']), array('class' => 'btn btn-default', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
			<?php echo $this->Html->link(__('<i class="fa fa-edit"></i>'), array('action' => 'edit', $registration['Registration']['id']), array('class' => 'btn btn-primary', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
			<?php echo $this->Form->postLink(__('<i class="fa fa-trash"></i>'), array('action' => 'delete', $registration['Registration']['id']), array('class' => 'btn btn-danger', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $registration['Registration']['id'])); ?>
		</div>
		</td>
	</tr>
<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$("#Table").dataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Portuguese-Brasil.json"
			},
			"pageLength": 20
		});
	});
</script>
