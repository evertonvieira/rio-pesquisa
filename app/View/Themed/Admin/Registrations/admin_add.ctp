<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<?php echo $this->Form->create('Registration', array('role' => 'form')); ?>
  <div class="box-body">
							<div class="form-group">
						<?php echo $this->Form->input('titular', array('label'=>'Titular:', 'class'=>'form-control', 'placeholder'=>'titular'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('data_nascimento', array('label'=>'Data_nascimento:', 'class'=>'form-control', 'placeholder'=>'data_nascimento'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('genero', array('label'=>'Genero:', 'class'=>'form-control', 'placeholder'=>'genero'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('email', array('label'=>'Email:', 'class'=>'form-control', 'placeholder'=>'email'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('telefone', array('label'=>'Telefone:', 'class'=>'form-control', 'placeholder'=>'telefone'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('nome_comercio', array('label'=>'Nome_comercio:', 'class'=>'form-control', 'placeholder'=>'nome_comercio'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('endereco', array('label'=>'Endereco:', 'class'=>'form-control', 'placeholder'=>'endereco'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('telefone_contato', array('label'=>'Telefone_contato:', 'class'=>'form-control', 'placeholder'=>'telefone_contato'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('whatsapp', array('label'=>'Whatsapp:', 'class'=>'form-control', 'placeholder'=>'whatsapp'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('email_contato', array('label'=>'Email_contato:', 'class'=>'form-control', 'placeholder'=>'email_contato'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('titulo_comercio', array('label'=>'Titulo_comercio:', 'class'=>'form-control', 'placeholder'=>'titulo_comercio'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('site', array('label'=>'Site:', 'class'=>'form-control', 'placeholder'=>'site'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('segmento', array('label'=>'Segmento:', 'class'=>'form-control', 'placeholder'=>'segmento'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('ext', array('label'=>'Ext:', 'class'=>'form-control', 'placeholder'=>'ext'));?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('description', array('label'=>'Description:', 'class'=>'form-control', 'placeholder'=>'description'));?>
					</div>
  </div>
  
					<div class="box-footer">
						<?php echo $this->Form->button('<i class="glyphicon glyphicon-saved"></i> Save data', array('type' => 'submit', 'class' => 'btn btn-large btn-primary', 'escape' => false)); ?>
					</div>
  <?php echo $this->Form->end(); ?>
</div>
