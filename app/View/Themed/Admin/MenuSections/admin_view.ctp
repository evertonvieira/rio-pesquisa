<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Editar menu de seção</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="col-md-12">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Editar'), array('action' => 'edit', $menuSection['MenuSection']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
	</div>

	<div class="box-body">
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
					<td><strong><?php echo __('id'); ?></strong></td>
					<td>
						<?php echo h($menuSection['MenuSection']['id']); ?>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td><strong><?php echo __('título do menu:'); ?></strong></td>
					<td>
						<?php echo h($menuSection['MenuSection']['name']); ?>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td><strong><?php echo __('menu pai'); ?></strong></td>
					<td>
						<?php echo $this->Html->link($menuSection['Menu']['name'], array('controller' => 'menus', 'action' => 'view', $menuSection['Menu']['id'])); ?>
						&nbsp;
					</td>
				</tr>
			</tbody>
		</table>
	</div>

</div>
<!-- /.box -->
