<!-- Default box -->
<script type="text/javascript">
  function formatCurrency(total) {
    var neg = false;
    if(total < 0) {
      neg = true;
      total = Math.abs(total);
    }
    return (neg ? "-R$ " : 'R$ ') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
  }
	function payments (type, month, year, status, element) {
		$.ajax({
      url: "<?php echo Router::url(array('controller'=>'payments','action'=>'getPayments', 'admin'=>true));?>/"+ type + '/'+ month + '/' + year + '/' + status + '/',
			type: 'POST',
			success: function(data){
  			$(`.${element}`).empty().append(formatCurrency(data));
			}
		});
	};
</script>
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Dashboard</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
  <div class="box-body">

    <div class="row">
      <div class="col-lg-4 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <script> setInterval(function() { payments('all', <?= date("m"); ?>, <?= date("Y"); ?>, 1, "consolidate") } , 1000);</script>
            <h3 class="consolidate"></h3>
            <p>Consolidado</p>
          </div>
          <div class="icon">
            <i class="ion ion-cash"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-4 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <script> setInterval(function() { payments(1, <?= date("m"); ?>, <?= date("Y"); ?>, 1, "debite") } , 1000);</script>
            <h3 class="debite"></h3>
            <p>Débito</p>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->

      <div class="col-lg-4 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <script> setInterval(function() { payments(0, <?= date("m"); ?>, <?= date("Y"); ?>, 1, "credite") } , 1000);</script>
            <h3 class="credite"></h3>
            <p>Crédito</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
    </div>


  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    Footer
  </div>
  <!-- /.box-footer-->
</div>
<!-- /.box -->
