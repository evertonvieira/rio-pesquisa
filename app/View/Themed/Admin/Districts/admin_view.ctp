
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><?= $title; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="box-body">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> editar'), array('action' => 'edit', $district['District']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
          <td><strong><?php echo __('Id'); ?></strong></td>
      		<td>
      			<?php echo h($district['District']['id']); ?>
      			&nbsp;
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('Title'); ?></strong></td>
      		<td>
      			<?php echo h($district['District']['title']); ?>
      			&nbsp;
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('City'); ?></strong></td>
      		<td>
      			<?php echo $this->Html->link($district['City']['title'], array('controller' => 'cities', 'action' => 'view', $district['City']['id']), array('class' => '')); ?>
      			&nbsp;
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('cadastrado'); ?></strong></td>
      		<td>
      			<?php echo h($district['District']['created']); ?>
      			&nbsp;
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('atuaizado'); ?></strong></td>
      		<td>
      			<?php echo h($district['District']['updated']); ?>
      			&nbsp;
      		</td>
        </tr>
      </tbody>
		</table>
	</div>
</div>
