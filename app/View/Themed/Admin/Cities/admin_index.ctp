<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title"><?= $title; ?></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="box-tools">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> nova cidade'), array('action' => 'add'), array('class' => 'btn btn-primary pull-right', 'escape' => false)); ?>
		</div>
		<div class="table-responsive">
			<table id="Table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th><?php echo __("id"); ?></th>
						<th><?php echo __("nome da cidade"); ?></th>
						<th><?php echo __("Estado"); ?></th>
						<th><?php echo __("cadastrado"); ?></th>
						<th><?php echo __("atualizado"); ?></th>
						<th width="80"><?php echo __('ações'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($cities as $city): ?>
						<tr>
							<td class="text-left"><?php echo h($city['City']['id']); ?></td>
							<td class="text-left"><?php echo h($city['City']['title']); ?></td>
							<td class="text-left">
								<?php echo $this->Html->link($city['State']['title'], array('controller' => 'states', 'action' => 'view', $city['State']['id'])); ?>
							</td>
							<td class="text-left"><?php echo $this->Formatacao->DataHora($city['City']['created']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->DataHora($city['City']['updated']); ?></td>
							<td class="text-center">
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $city['City']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $city['City']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
								<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $city['City']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $city['City']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
