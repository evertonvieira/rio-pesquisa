let gulp = require('gulp');
let csso = require('gulp-csso');
let concat = require('gulp-concat');
let jsmin = require('gulp-jsmin');

const dirCss = 'webroot/css';
const dirJs = 'webroot/js';

gulp.task('css', () => {
  gulp.src(
    [
      dirCss + '/bootstrap.css',
      dirCss + '/bootstrap-theme.css',
      dirCss + '/dataTables.bootstrap.css',
      dirCss + '/select2.min.css',
      dirCss + '/style.css',
    ]
  )
  .pipe(csso())
  .pipe(concat('assets.min.css'))
  .pipe(gulp.dest(dirCss))
  console.log("Concatenação e minifcação de css concluída!");
});

gulp.task('js', () => {
  gulp.src(
    [
      dirJs + '/jquery-2.2.0.js',
      dirJs + '/bootstrap.js',
      dirJs + '/app.min.js',
      dirJs + '/pace.min.js',
      dirJs + '/jquery.dataTables.js',
      dirJs + '/dataTables.bootstrap.js',
      dirJs + '/jquery.mask.min.js',
      dirJs + '/select2.full.js',
      dirJs + '/scripts.js',
    ]
  )
  .pipe(jsmin())
  .pipe(concat('assets.min.js'))
  .pipe(gulp.dest(dirJs))
  console.log("Concatenação e minifcação de js concluída!");
});
