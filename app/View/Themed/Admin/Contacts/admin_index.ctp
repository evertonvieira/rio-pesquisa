<div class="row">
	<div class="col-xs-12">
    <div class="box box-primary">
			<div class="box-body">
				<table id="Contacts" class="table table-bordered table-striped table-responsive">
					<thead>
						<tr>
							<th class="text-left"><?php echo __("id"); ?></th>
							<th class="text-left"><?php echo __("nome"); ?></th>
							<th class="text-left"><?php echo __("e-mail"); ?></th>
							<th class="text-left"><?php echo __("telefone"); ?></th>
							<th class="text-left"><?php echo __("whatsapp"); ?></th>
							<th class="text-left"><?php echo __("assunto"); ?></th>
							<th class="text-left"><?php echo __("mensagem"); ?></th>
							<th class="text-left"><?php echo __("lido?"); ?></th>
							<th class="text-left"><?php echo __("enviado"); ?></th>
							<th class="text-left"><?php echo __("atualizado"); ?></th>
							<th width="30" class="text-left"><?php echo __('ações'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($contacts as $contact): ?>
						<tr>
							<td class="text-left"><?php echo h($contact['Contact']['id']); ?></td>
							<td class="text-left"><?php echo h($contact['Contact']['name']); ?></td>
							<td class="text-left"><?php echo h($contact['Contact']['email']); ?></td>
							<td class="text-left"><?php echo h($contact['Contact']['phone']); ?></td>
							<td class="text-left"><?php echo h($contact['Contact']['whatsapp']); ?></td>
							<td class="text-left"><?php echo h($contact['Contact']['subject']); ?></td>
							<td class="text-left"><?php echo h($contact['Contact']['message']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->featured($contact['Contact']['status']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->dataHora($contact['Contact']['created']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->dataHora($contact['Contact']['updated']); ?></td>
							<td class="text-left">
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $contact['Contact']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
								<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $contact['Contact']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $contact['Contact']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$("#Contacts").dataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Portuguese-Brasil.json"
			}
		});
	});
</script>
