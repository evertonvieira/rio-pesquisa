
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><?= $title; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="box-body">
		<div class="box-tools" style="margin-bottom:15px; overflow: hidden;">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> editar'), array('action' => 'edit', $company['Company']['id']), array('class' => 'btn btn-primary pull-right', 'escape' => false)); ?>
		</div>
    <div class="row">
      <div class="col-md-3">
        <!-- Profile Image -->
        <div class="box">
          <div class="box-body box-profile">
            <?php echo $this->Html->image("Company/{$company['Company']['id']}.{$company['Company']['ext']}", ['class'=>'profile-user-img img-responsive img-circle', 'alt'=>$company['Company']['title']] );?>
            <h3 class="profile-username text-center"><?= $company['Company']['title']; ?></h3>
            <p class="text-muted text-center"><?= $company['Segment']['title']; ?></p>
            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Plano</b> <p class="pull-right text-muted"><?= $company['Plan']['name']; ?></p>
              </li>
              <li class="list-group-item">
                <b>CNPJ</b> <p class="pull-right text-muted"><?= $company['Company']['cnpj']; ?></p>
              </li>
              <li class="list-group-item">
                <b>Status</b> <p class="pull-right text-muted"><?= $this->Formatacao->active($company['Company']['active']); ?></p>
              </li>
            </ul>
            <?php echo $this->Html->link('<b>Ver Companhia</b>', array('admin'=>false, 'controller' => 'companies', 'action'=>'view', 'slug'=> $company['Company']['slug'], 'segment'=>$company['Segment']['slug']), array('target'=>'_blank', 'class'=>'btn btn-success btn-block', 'escape' => false));?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- About Me Box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Sobre a companhia</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <strong><i class="fa fa-map-marker margin-r-5"></i>Localização</strong>
            <p class="text-muted"><?= $company['Company']['rua']; ?>, n° <?= $company['Company']['number']; ?>, <?= $company['District']['title']; ?>, <?= $company['District']['City']['title']; ?></p>
            <strong><i class="fa fa-phone margin-r-5"></i>Telefone</strong>
            <p class="text-muted"><?= $company['Company']['telefone']; ?></p>
            <strong><i class="fa fa-mobile margin-r-5"></i>Whatsapp</strong>
            <p class="text-muted"><?= $company['Company']['whatsapp']; ?></p>
            <strong><i class="fa fa-envelope margin-r-5"></i>E-mail</strong>
            <p class="text-muted"><?= $company['Company']['email']; ?></p>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>

      <div class="col-md-9">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Dados da companhia</a></li>
            <li class=""><a href="#customer" data-toggle="tab" aria-expanded="false">Dados do responsável</a></li>
            <li class=""><a href="#payments" data-toggle="tab" aria-expanded="false">Dados de mensalidade</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="activity">
              <div class="box-header with-border">
                <i class="fa fa-file-text-o margin-r-5"></i>
                <h3 class="box-title">Descrição da companhia</h3>
              </div>
              <!-- /.box-header -->
              <div class="t-body">
                <div class="jumbotron">
                  <?= $company['Company']['description']; ?>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-header with-border">
                <i class="fa fa-map-marker margin-r-5"></i>
                <h3 class="box-title">Mapa</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div id="map" style="height:400px;">
                  <?= $company['Company']['mapa']; ?>
                </div>
              </div>
              <!-- /.box-body -->

            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="customer">
              <div class="box-header with-border">
                <i class="fa fa-file-text-o margin-r-5"></i>
                <h3 class="box-title">Dados do responsável</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <?php if ($company['Customer']['obs']): ?>
                  <p>
                    <?= $company['Customer']['obs']; ?>
                  </p>
                <?php endif; ?>
                <ul class="list-group">
                  <li class="list-group-item">
                    <b>Nome</b>
                      <?php echo $this->Html->link($company['Customer']['titular'], array('plugin'=>false, 'controller' => 'customers', 'action'=>'view', $company['Customer']['id']), array('class'=>'pull-right','escape' => false));?>
                  </li>
                  <li class="list-group-item">
                    <b>CPF</b> <a class="pull-right"><?= $company['Customer']['cpf']; ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Data de nascimento</b> <a class="pull-right"><?= $this->Formatacao->Data($company['Customer']['data_nascimento']); ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>E-mail</b> <a class="pull-right"><?= $company['Customer']['email']; ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Telefone Fixo</b> <a class="pull-right"><?= $company['Customer']['phone_fixo']; ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Celular</b> <a class="pull-right"><?= $company['Customer']['celular']; ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Gênero</b> <a class="pull-right"><?= $this->Formatacao->genero($company['Customer']['genero']); ?></a>
                  </li>
                </ul>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="payments">
                <table id="Table" class="table table-bordered table-striped table-responsive">
            			<thead>
            				<tr>
            					<th><?php echo __("id"); ?></th>
            					<th><?php echo __("status"); ?></th>
            					<th><?php echo __("vencimento"); ?></th>
                      <th><?php echo __("data do pagamento"); ?></th>
            					<th><?php echo __("valor (R$)"); ?></th>
            					<th><?php echo __("método de pagamento"); ?></th>
            				</tr>
            			</thead>
            			<tbody>
            				<?php foreach ($company['Payment'] as $payment): ?>
            					<tr>
            						<td class="text-left"><?php echo h($payment['id']); ?></td>
            						<td class="text-left"><?php echo $this->Formatacao->statusMoney($payment['status']); ?></td>
            						<td class="text-left"><?php echo $this->Formatacao->Data($payment['vencimento']); ?></td>
                        <td class="text-left"><?php echo $this->Formatacao->Data($payment['data_pago']); ?></td>
            						<td class="text-left"><?php echo $this->Formatacao->moeda($payment['value']); ?></td>
            						<td class="text-left"><?php echo $this->Formatacao->methodPayment($payment['method']); ?></td>
            					</tr>
            				<?php endforeach; ?>
            			</tbody>
            		</table>
            </div>
            <!-- /.tab-pane -->


          </div>
          <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
      </div>

    </div>



	</div>
</div>
