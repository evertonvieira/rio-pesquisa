<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title"><?= $title; ?></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="box-tools">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> novo comércio'), array('action' => 'add'), array('class' => 'btn btn-primary pull-right', 'escape' => false)); ?>
		</div>
		<div class="table-responsive">
			<table id="Table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th><?php echo __("id"); ?></th>
						<th><?php echo __("nome do comércio"); ?></th>
						<th><?php echo __("status"); ?></th>
						<th><?php echo __("cliente/responsável"); ?></th>
						<th><?php echo __("segmento"); ?></th>
						<th><?php echo __("plano"); ?></th>
						<th><?php echo __("bairro"); ?></th>
						<th><?php echo __("cadatrado"); ?></th>
						<th><?php echo __("atualizado"); ?></th>
						<th width="100"><?php echo __('ações'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($companies as $company):?>
						<tr>
							<td><?php echo h($company['Company']['id']); ?></td>
							<td ><?php echo h($company['Company']['title']); ?></td>
							<td><?php echo $this->Formatacao->active($company['Company']['active']); ?></td>
							<td>
								<?php echo $this->Html->link($company['Customer']['titular'], array('controller' => 'customers', 'action' => 'view', $company['Customer']['id'])); ?>
							</td>
							<td class="text-left">
								<?php echo $this->Html->link($company['Segment']['title'], array('controller' => 'segments', 'action' => 'view', $company['Segment']['id'])); ?>
							</td>
							<td class="text-left">
								<?php echo $this->Html->link($company['Plan']['name'], array('controller' => 'plans', 'action' => 'view', $company['Plan']['id'])); ?>
							</td>
							<td class="text-left">
								<?php echo $this->Html->link($company['District']['title'], array('controller' => 'districts', 'action' => 'view', $company['District']['id'])); ?>
							</td>
							<td class="text-left"><?php echo $this->Formatacao->dataHora($company['Company']['created']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->dataHora($company['Company']['updated']); ?></td>
							<td class="text-center">
								<?php echo $this->Html->link(__('<i class="fa fa-image"></i>'), array('action' => 'images', $company['Company']['id']), array('class' => 'btn btn-success btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>

								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $company['Company']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $company['Company']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
								<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $company['Company']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $company['Company']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
