<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title"><?= $title; ?></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="box-tools">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> novo pagamento'), array('action' => 'add'), array('class' => 'btn btn-primary pull-right', 'escape' => false)); ?>
		</div>
		<div class="table-responsive">
			<table id="Table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th><?php echo __("id"); ?></th>
						<th><?php echo __("tipo de pagamento"); ?></th>
						<th><?php echo __("status"); ?></th>
						<th><?php echo __("vencimento"); ?></th>
						<th><?php echo __("valor (R$)"); ?></th>
						<th><?php echo __("método de pagamento"); ?></th>
						<th><?php echo __("atualizado"); ?></th>
						<th width="80"><?php echo __('ações'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($payments as $payment): ?>
						<tr>
							<td class="text-left"><?php echo h($payment['Payment']['id']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->typePayment($payment['Payment']['type']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->statusMoney($payment['Payment']['status']); ?></td>
							<td class="text-left"><?php echo h($payment['Payment']['vencimento']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->moeda($payment['Payment']['value']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->methodPayment($payment['Payment']['method']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->DataHora($payment['Payment']['updated']); ?></td>
							<td class="text-center">
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $payment['Payment']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $payment['Payment']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
								<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $payment['Payment']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $payment['Payment']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
