
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><= $title; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="box-body">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Edit'), array('action' => 'edit', $payment['Payment']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($payment['Payment']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Type'); ?></strong></td>
		<td>
			<?php echo h($payment['Payment']['type']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Status'); ?></strong></td>
		<td>
			<?php echo h($payment['Payment']['status']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Vencimento'); ?></strong></td>
		<td>
			<?php echo h($payment['Payment']['vencimento']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Value'); ?></strong></td>
		<td>
			<?php echo h($payment['Payment']['value']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Company'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($payment['Company']['title'], array('controller' => 'companies', 'action' => 'view', $payment['Company']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Plan'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($payment['Plan']['name'], array('controller' => 'plans', 'action' => 'view', $payment['Plan']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Method'); ?></strong></td>
		<td>
			<?php echo h($payment['Payment']['method']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Obs'); ?></strong></td>
		<td>
			<?php echo h($payment['Payment']['obs']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($payment['Payment']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Updated'); ?></strong></td>
		<td>
			<?php echo h($payment['Payment']['updated']); ?>
			&nbsp;
		</td>
</tr>			</tbody>
		</table>
	</div>

	
</div>
