<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Menus</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="box-tools">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> New Menu'), array('action' => 'add'), array('class' => 'btn btn-primary pull-right', 'escape' => false)); ?>
		</div>
		<div class="table-responsive">
			<table id="Table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th class="text-left"><?php echo __("id"); ?></th>
						<th class="text-left"><?php echo __("name"); ?></th>
						<th width="80" class="text-left"><?php echo __('ações'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($menus as $menu): ?>
						<tr>
							<td><?php echo h($menu['Menu']['id']); ?></td>
							<td><?php echo h($menu['Menu']['name']); ?></td>
							<td class="text-center">
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $menu['Menu']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $menu['Menu']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
								<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $menu['Menu']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $menu['Menu']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
