<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Menu</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>

	<div class="box-body">
    <div class="box-tools pull-right">
      <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Editar'), array('action' => 'edit', $menu['Menu']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
    </div>
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
					<td><strong><?php echo __('Id'); ?></strong></td>
					<td>
						<?php echo h($menu['Menu']['id']); ?>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td><strong><?php echo __('Name'); ?></strong></td>
					<td>
						<?php echo h($menu['Menu']['name']); ?>
						&nbsp;
					</td>
				</tr>
			</tbody>
		</table>
	</div>

</div>
<!-- /.box -->
