$(document).ready( ()=> {

  //masks inputs
  $(".Moeda").mask('000000.00', {reverse: true});
  $(".DataNascimento").mask('00/00/0000');
  $(".Cpf").mask('000.000.000-00', {reverse: false});
  $(".TelefoneFixo").mask('(00) 0000-0000');
  $(".Cnpj").mask('00.000.000/0000-00');
  $(".Celular").mask('(00) 00000-0000');

  $('li.dropdown').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(100);
  }, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(100);
  });

  //select2
  $('.SelectTag').select2();


  //dataTable
	$("#Table").dataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Portuguese-Brasil.json"
		},
		"pageLength": 30
	});


})
