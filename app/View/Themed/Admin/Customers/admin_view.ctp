<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><?= $title; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="box-body">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> editar'), array('action' => 'edit', $customer['Customer']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
		<table class="table table-bordered table-striped">
			<tbody>
        <tr>
          <td><strong><?php echo __('id'); ?></strong></td>
      		<td>
      			<?php echo h($customer['Customer']['id']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('nome do titular'); ?></strong></td>
      		<td>
      			<?php echo h($customer['Customer']['titular']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('CPF'); ?></strong></td>
      		<td>
      			<?php echo h($customer['Customer']['cpf']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('Data de Nascimento'); ?></strong></td>
      		<td>
      			<?php echo h($customer['Customer']['data_nascimento']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('e-mail'); ?></strong></td>
      		<td>
      			<?php echo h($customer['Customer']['email']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('telefone fixo'); ?></strong></td>
      		<td>
      			<?php echo h($customer['Customer']['phone_fixo']); ?>

      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('celular'); ?></strong></td>
      		<td>
      			<?php echo h($customer['Customer']['celular']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('Gênero'); ?></strong></td>
      		<td>
      			<?php echo $this->Formatacao->genero($customer['Customer']['genero']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('observação sobre o cliente'); ?></strong></td>
      		<td>
      			<?php echo h($customer['Customer']['obs']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('cadastrado'); ?></strong></td>
      		<td>
      			<?php echo $this->Formatacao->DataHora($customer['Customer']['created']); ?>
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('atualizado'); ?></strong></td>
      		<td>
      			<?php echo $this->Formatacao->DataHora($customer['Customer']['updated']); ?>
      		</td>
        </tr>
      </tbody>
		</table>
	</div>
</div>
