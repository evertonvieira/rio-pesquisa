
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><?= $title; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="box-body">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Edit'), array('action' => 'edit', $outdoor['Outdoor']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($outdoor['Outdoor']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Title'); ?></strong></td>
		<td>
			<?php echo h($outdoor['Outdoor']['title']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Link'); ?></strong></td>
		<td>
			<?php echo h($outdoor['Outdoor']['link']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Date'); ?></strong></td>
		<td>
			<?php echo h($outdoor['Outdoor']['date']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Ext'); ?></strong></td>
		<td>
			<?php echo h($outdoor['Outdoor']['ext']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Company'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($outdoor['Company']['title'], array('controller' => 'companies', 'action' => 'view', $outdoor['Company']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Plan'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($outdoor['Plan']['name'], array('controller' => 'plans', 'action' => 'view', $outdoor['Plan']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Position'); ?></strong></td>
		<td>
			<?php echo h($outdoor['Outdoor']['position']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Active'); ?></strong></td>
		<td>
			<?php echo h($outdoor['Outdoor']['active']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($outdoor['Outdoor']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Updated'); ?></strong></td>
		<td>
			<?php echo h($outdoor['Outdoor']['updated']); ?>
			&nbsp;
		</td>
</tr>			</tbody>
		</table>
	</div>


</div>
