<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $title; ?></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="box-tools">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> novo outdoor'), array('action' => 'add'), array('class' => 'btn btn-primary pull-right', 'escape' => false)); ?>
		</div>
		<div class="table-responsive">
			<table id="Table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th><?php echo __("id"); ?></th>
						<th><?php echo __("titulo"); ?></th>
						<th><?php echo __("data"); ?></th>
						<th><?php echo __("Companhia"); ?></th>
						<th><?php echo __("plano"); ?></th>
						<th><?php echo __("status"); ?></th>
						<th><?php echo __("posição"); ?></th>
						<th><?php echo __("cadastrado"); ?></th>
						<th><?php echo __("atualizado"); ?></th>
						<th width="80"><?php echo __('ações'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($outdoors as $outdoor): ?>
						<tr>
							<td class="text-left"><?php echo h($outdoor['Outdoor']['id']); ?></td>
							<td class="text-left"><?php echo h($outdoor['Outdoor']['title']); ?></td>
							<td class="text-left"><?php echo h($outdoor['Outdoor']['date']); ?></td>
							<td class="text-left">
								<?php echo $this->Html->link($outdoor['Company']['title'], array('controller' => 'companies', 'action' => 'view', $outdoor['Company']['id'])); ?>
							</td>
							<td class="text-center">
								<?php echo $this->Html->link($outdoor['Plan']['name'], array('controller' => 'plans', 'action' => 'view', $outdoor['Plan']['id'])); ?>
							</td>
							<td class="text-left"><?php echo $this->Formatacao->active($outdoor['Outdoor']['active']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->slidePosition($outdoor['Outdoor']['position']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->DataHora($outdoor['Outdoor']['created']); ?></td>
							<td class="text-left"><?php echo $this->Formatacao->DataHora($outdoor['Outdoor']['updated']); ?></td>
							<td class="text-center">
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $outdoor['Outdoor']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $outdoor['Outdoor']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
								<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $outdoor['Outdoor']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $outdoor['Outdoor']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
