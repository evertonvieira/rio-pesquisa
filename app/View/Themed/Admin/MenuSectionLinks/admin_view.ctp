<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Links de seção</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="col-md-12">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> editar'), array('action' => 'edit', $menuSectionLink['MenuSectionLink']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
	</div>

	<div class="box-body">
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
					<td><strong><?php echo __('id'); ?></strong></td>
					<td>
						<?php echo h($menuSectionLink['MenuSectionLink']['id']); ?>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td><strong><?php echo __('name'); ?></strong></td>
					<td>
						<?php echo h($menuSectionLink['MenuSectionLink']['name']); ?>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td><strong><?php echo __('alias'); ?></strong></td>
					<td>
						<?php echo h($menuSectionLink['MenuSectionLink']['alias']); ?>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td><strong><?php echo __('seção pai'); ?></strong></td>
					<td>
						<?php echo $this->Html->link($menuSectionLink['MenuSection']['name'], array('controller' => 'menu_sections', 'action' => 'view', $menuSectionLink['MenuSection']['id'])); ?>
						&nbsp;
					</td>
				</tr>
			</tbody>
		</table>
	</div>

</div>
<!-- /.box -->
