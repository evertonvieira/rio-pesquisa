<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Links de seção</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="box-tools">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> novo link de seção'), array('action' => 'add'), array('class' => 'btn btn-primary pull-right', 'escape' => false)); ?>
		</div>
		<div class="table-responsive">
			<table id="Table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th><?php echo __("id"); ?></th>
						<th><?php echo __("name"); ?></th>
						<th><?php echo __("alias"); ?></th>
						<th><?php echo __("icon"); ?></th>
						<th><?php echo __("sessão pai"); ?></th>
						<th width="80"><?php echo __('ações'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($menuSectionLinks as $menuSectionLink): ?>
						<tr>
							<td><?php echo h($menuSectionLink['MenuSectionLink']['id']); ?></td>
							<td><?php echo h($menuSectionLink['MenuSectionLink']['name']); ?></td>
							<td><?php echo h($menuSectionLink['MenuSectionLink']['alias']); ?></td>
							<td><i class="<?php echo h($menuSectionLink['MenuSectionLink']['icon']); ?>"></i></td>
							<td>
								<?php echo $this->Html->link($menuSectionLink['MenuSection']['name'], array('controller' => 'menu_sections', 'action' => 'view', $menuSectionLink['MenuSection']['id'])); ?>
							</td>
							<td class="text-center">
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $menuSectionLink['MenuSectionLink']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $menuSectionLink['MenuSectionLink']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
								<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $menuSectionLink['MenuSectionLink']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $menuSectionLink['MenuSectionLink']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
