<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Usuários</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="box-tools">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> novo usuário'), array('action' => 'add'), array('class' => 'btn btn-primary pull-right', 'escape' => false)); ?>
		</div>
		<div class="table-responsive">
			<table id="Table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th><?php echo __("id"); ?></th>
						<th><?php echo __("nome"); ?></th>
						<th><?php echo __("email"); ?></th>
						<th><?php echo __("status"); ?></th>
						<th><?php echo __("criado"); ?></th>
						<th><?php echo __("atualizado"); ?></th>
						<th width="80"><?php echo __('ações'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($users as $user):?>
						<tr>
							<td><?php echo h($user['User']['id']); ?></td>
							<td><?php echo h($user['User']['name']); ?></td>
							<td><?php echo h($user['User']['email']); ?></td>
							<td><?php echo $this->Formatacao->active($user['User']['status']); ?></td>
							<td><?php echo $this->Formatacao->DataHora($user['User']['created']); ?></td>
							<td><?php echo $this->Formatacao->DataHora($user['User']['updated']); ?></td>
							<td class="text-center">
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
								<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
								<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
