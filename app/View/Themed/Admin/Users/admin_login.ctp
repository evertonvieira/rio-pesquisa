<?php  echo $this->Session->flash('auth');  ?>
<?php echo $this->Form->create('User', array('class'=>'users form"'));?>
  <h3>Por favor, entre com seus dados:</h3>
  <hr>
  <div class="form-group has-feedback">
    <?php echo $this->Form->input('email',array('type'=>'email', 'class'=>'form-control', 'placeholder'=>'e-mail', 'label'=>false)); ?>
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
  </div>

  <div class="form-group has-feedback">
    <?php echo $this->Form->input('password',array('type'=>'password', 'class'=>'form-control', 'label'=>false, 'placeholder'=>'password')); ?>
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  <?php echo $this->Form->button('Sign In', array('type' => 'submit', 'class' => 'btn btn-primary btn-block btn-flat', 'escape' => false)); ?>
  <hr />
  <a href="#" class="text-left">Esqueceu sua senha?</a>
<?php echo $this->Form->end(); ?>
