<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $title; ?></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="box-tools">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> nova página'), array('action' => 'add'), array('class' => 'btn btn-primary pull-right', 'escape' => false)); ?>
		</div>
		<div class="table-responsive">
  		<table id="Table" class="table table-bordered table-striped">
	      <thead>
	        <tr>
	          <th class="text-left"><?php echo __("id"); ?></th>
	          <th class="text-left"><?php echo __("titulo"); ?></th>
	          <th class="text-left"><?php echo __("slug"); ?></th>
	          <th class="text-left"><?php echo __("view"); ?></th>
	          <th class="text-left"><?php echo __("status"); ?></th>
	          <th class="text-left"><?php echo __("cadastrado"); ?></th>
	          <th class="text-left"><?php echo __("atualizado"); ?></th>
	          <th width="80"><?php echo __('Actions'); ?></th>
	        </tr>
	      </thead>
	      <tbody>
	        <?php foreach ($pages as $page): ?>
	          <tr>
	            <td class="text-left"><?php echo h($page['Page']['id']); ?></td>
	            <td class="text-left"><?php echo h($page['Page']['title']); ?></td>
	            <td class="text-left"><?php echo h($page['Page']['slug']); ?></td>
	            <td class="text-left"><?php echo h($page['Page']['view']); ?></td>
	            <td class="text-left"><?php echo $this->Formatacao->active($page['Page']['status']); ?></td>
	            <td class="text-left"><?php echo $this->Formatacao->dataHora($page['Page']['created']); ?></td>
	            <td class="text-left"><?php echo $this->Formatacao->dataHora($page['Page']['updated']); ?></td>
	            <td class="text-center">
	              <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $page['Page']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
	              <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $page['Page']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
	              <?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $page['Page']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $page['Page']['id'])); ?>
	            </td>
	          </tr>
	        <?php endforeach; ?>
	      </tbody>
	    </table>
		</div>
	</div>
</div>
