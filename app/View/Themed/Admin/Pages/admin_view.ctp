<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><?= $title; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="box-body">
		<div class="box-tools pull-right">
      <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> editar'), array('action' => 'edit', $page['Page']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
    <table id="Pages" class="table table-bordered table-striped">
      <tbody>
        <tr>
          <td><strong><?php echo __('Id'); ?></strong></td>
          <td>
            <?php echo h($page['Page']['id']); ?>
            &nbsp;
          </td>
        </tr>
        <tr>
          <td><strong><?php echo __('Title'); ?></strong></td>
          <td>
            <?php echo h($page['Page']['title']); ?>
            &nbsp;
          </td>
        </tr>
        <tr>
          <td><strong><?php echo __('Slug'); ?></strong></td>
          <td>
            <?php echo h($page['Page']['slug']); ?>
            &nbsp;
          </td>
        </tr>
        <tr>
          <td><strong><?php echo __('Status'); ?></strong></td>
          <td>
            <?php echo $this->Formatacao->active($page['Page']['status']); ?>
            &nbsp;
          </td>
        </tr>
        <tr>
          <td><strong><?php echo __('View'); ?></strong></td>
          <td>
            <?php echo h($page['Page']['view']); ?>
            &nbsp;
          </td>
        </tr>
        <tr>
          <td><strong><?php echo __('Body'); ?></strong></td>
          <td>
            <?php echo $page['Page']['body']; ?>
            &nbsp;
          </td>
        </tr>
        <tr>
          <td><strong><?php echo __('Created'); ?></strong></td>
          <td>
            <?php echo $this->Formatacao->dataCompleta($page['Page']['created']); ?>
            &nbsp;
          </td>
        </tr>
        <tr>
          <td><strong><?php echo __('Updated'); ?></strong></td>
          <td>
            <?php echo $this->Formatacao->dataCompleta($page['Page']['updated']); ?>
            &nbsp;
          </td>
        </tr>
      </tbody>
    </table>
	</div>
</div>
