<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title"><?= $title; ?></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				<i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="box-tools">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> novo plano'), array('action' => 'add'), array('class' => 'btn btn-primary pull-right', 'escape' => false)); ?>
		</div>
		<div class="table-responsive">
			<table id="Table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th><?php echo __("id"); ?></th>
						<th><?php echo __("nome do plano"); ?></th>
						<th><?php echo __("preço"); ?></th>
						<th><?php echo __("descrição"); ?></th>
						<th><?php echo __("criado"); ?></th>
						<th><?php echo __("atualizado"); ?></th>
						<th width="80"><?php echo __('ações'); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($plans as $plan): ?>
					<tr>
						<td class="text-left"><?php echo h($plan['Plan']['id']); ?></td>
						<td class="text-left"><?php echo h($plan['Plan']['name']); ?></td>
						<td class="text-left"><?php echo $this->Formatacao->moeda($plan['Plan']['price']); ?></td>
						<td class="text-left"><?php echo h($plan['Plan']['description']); ?></td>
						<td class="text-left"><?php echo $this->Formatacao->DataHora($plan['Plan']['created']); ?></td>
						<td class="text-left"><?php echo $this->Formatacao->DataHora($plan['Plan']['updated']); ?></td>
						<td class="text-center">
							<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $plan['Plan']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>
							<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $plan['Plan']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>
							<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $plan['Plan']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', $plan['Plan']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
