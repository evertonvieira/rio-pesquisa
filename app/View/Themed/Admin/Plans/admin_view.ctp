<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><?= $title; ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
	<div class="box-body">
		<div class="box-tools pull-right">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> editar'), array('action' => 'edit', $plan['Plan']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
		</div>
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
          <td><strong><?php echo __('id'); ?></strong></td>
          <td>
  			     <?php echo h($plan['Plan']['id']); ?>
		        &nbsp;
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('nome'); ?></strong></td>
      		<td>
      			<?php echo h($plan['Plan']['name']); ?>
      			&nbsp;
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('preço'); ?></strong></td>
      		<td>
      			<?php echo $this->Formatacao->moeda($plan['Plan']['price']); ?>
      			&nbsp;
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('descrição'); ?></strong></td>
      		<td>
      			<?php echo h($plan['Plan']['description']); ?>
      			&nbsp;
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('cadastrado'); ?></strong></td>
      		<td>
      			<?php echo $this->Formatacao->DataHora($plan['Plan']['created']); ?>
      			&nbsp;
      		</td>
        </tr>
        <tr>
          <td><strong><?php echo __('atualizado'); ?></strong></td>
      		<td>
      			<?php echo $this->Formatacao->DataHora($plan['Plan']['updated']); ?>
      			&nbsp;
      		</td>
        </tr>
      </tbody>
		</table>
	</div>
</div>
